# **Uneyama-Doi model (UDM)** 
    GPU-accelerated continuum model simulations of polymer blends and solutions

UDM offers continuum model simulations of polymer blends and solutions based on:

- [ ] [Uneyama, T., & Doi, M. (2005). Density functional theory for block copolymer melts and blends. *Macromolecules, 38*(1), 196-205](https://doi.org/10.1021/ma049385m)
- [ ] [Uneyama, T. (2007). Density functional simulation of spontaneous formation of vesicle in block copolymer solutions. *The Journal of chemical physics, 126*(11)](https://doi.org/10.1063/1.2463426)

The numerical implementation is based on:

- [ ] [Häfner, G., & Müller, M. (2023). Reaction-driven assembly: controlling changes in membrane topology by reaction cycles. *Soft Matter, 19*(38), 7281-7292](https://doi.org/10.1039/D3SM00876B)

The specifics of the numerical implementation are also compiled in the file `model_introduction.pdf`. Here we will give a quick introduction into installation (using *CMake*) and 
usage.

## License

UDM is free and open source software. It is licensed with the GNU Lesser General
Public License. For more details read COPYING and COPYING.LESSER.

## Authors

Gregor Häfner

## Dependencies

- [ ] CUDA 
- [ ] CMake
- [ ] HDF5
- [ ] h5py (not required to build, install or run the UDM, but to prepare input files)

## Install

 UDM is designed for the out of source compilation using *CMake*. So *CMake* is
 required to build the UDM. This section will step by step guide you to build and
 install UDM.

1. Git checkout your desired branch our tag of UDM. Suggested is the latest
tag or the "master" branch.

     `git checkout master`

2. Create your build directory. And enter it.

     `mkdir build`

     `cd build`

3. Configure *CMake*. This step depends on the type of build:
     - If you want a standard build with your standard compiler and you have system
 wide installed parallel hdf5.

         `cmake -DCMAKE_INSTALL_PREFIX=/path/to/UDMinstall ../`

     - If you are not using a system wide *HDF5* installation set
 the `HDF5_ROOT` variable.

         `HDF5_ROOT=/path/to/hdf5install \`
         
         `cmake -DCMAKE_INSTALL_PREFIX=/path/to/UDMinstall ../`

  It is important that the path is `HDF5_ROOT` is absolute.
  Sometimes problems occur and the wrong library of *HDF5* is found.
  Try to guide cmake appropriately.

4. Now you can easily modify the options of **UDM** build using

     `ccmake .`

5. Build **UDM**:

     `make`

6. Install **UDM**:

     `make install`

  Now you find the `UDM` executable at `/path/to/UDMinstall/bin` and if you
  haven't deactivated the installation of the python scripts you can find them
  at `/path/to/UDMinstall/python-script`.
  You may want to set your `PATH` accordingly.

  For builds on commonly used clusters try, if you find an appropriate
  file in `environment`.

# Example Usage

Example input files can be found in the example directory.

1. In the location of your .xml-input file (e.g. `example.xml` found in the directory `example/`) 
   symbolically link `ConfGen.py` and `UDM` executable (not required, just for 
   usability):

    `ln -s /path/to/UDMinstall/python-script/ConfGen.py`

    `ln -s /path/to/UDMinstall/bin/UDM`
 
2. Create yourself an *HDF5* input configuration:

    `python3 ConfGen.py -i example.xml`

3. This creates the .h5-files (`example.h5`) with the simulation configuration.   

4. Now you can finally run your simulation by using

    `./UDM example.h5 [GPU_id]`

5. The simulation can be stopped at any time and will be picked up at the 
   last saved location.

6. If a set of parameters is supposed to be updated. ConfGen.py can be used to
   do so:

    `python3 ConfGen.py -i example.xml [-o example.h5] --update`

7. The result can be used regularly, with updated parameters as specified in the
   .xml-file.

8. If an exisiting setup is to be overwritten, specify so in the setup:
   
    `python3 ConfGen.py -i example.xml [-o example_thats_overwritten.h5] --overwrite`

9. More options, such as saving the chemical potentials and the free energy can
   be seen using 

    `python3 ConfGen.py --help`

10. The saved density fields are accessible in the dataset "phi" of the .h5-file.

11. Using the open source software *paraview* allows to show the results. 
  For this an xdmf file needs to be created:

    `python3 /path/to/UDMinstall/python-script/xmf_creator.py -i example.h5`

12. This creates the file `example_paraview.xmf` which can be opened in
   paraview and the data can be displayed.
