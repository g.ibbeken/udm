/* Copyright (c) 2020-2022 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

/*---Modules: ------------------------------------------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "phase.h"
#include "helper.h"
#include "saving.h"
#include "initialize_finalize.h"
#include "propagate.h"

/*---Main: ------------------------------------------------------------------------------------------------------------------------------------------------------*/
int main(int argc, char **argv)
{
    //First create structure: (phase and components)
    PHASE phase, *p=&phase;
    initialize_phase(p, argc, argv);
    
    time_t simulation_start_t;
    udm_scalar diff;
    time(&simulation_start_t);

    //run simulation, check error flag.
    switch(simulate(p))
    {
        case 0:
            diff = difftime(time(NULL), simulation_start_t);
            printf("Simulation completed succesfully in t=%.0fh %dm %ds. Freeing memory.\n", diff/3600, ((int)diff%3600)/60, ((int)diff%3600)%60);
            break;
        case 2:
            printf("\n===============================================================================================================================\n");
            printf("Nans appeared, simulation aborts.\n");
            printf("===============================================================================================================================\n\n");

            break;
        default:
            diff = difftime(time(NULL), simulation_start_t);
            printf("Simulation aborted with exit code 1 after t=%.0fh %dm %ds. Freeing memory.\n", diff/3600, ((int)diff%3600)/60, ((int)diff%3600)%60);
    }

    //Close hdf5 variables:
    finalize_phase(p);
    return(0);
}
