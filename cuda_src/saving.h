/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SAVING_H
#define SAVING_H

#include <hdf5.h>

/*---Function Declarations: ---------------------------------------------------------------------------------------------------------------------------------------------------*/
/** Read all simulation parameters and the concentration fields from the input
 * file, as given to the simulation as the first argument and saved in p->fname.
 * This function already allocates a lot (if not all) of the CPU memory. 
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void read_hdf5(struct PHASE *p);

/** Write the current morphology (all concentration fields and (if specified the 
 * chemical potentials, free-energy density and free energy, etc.) to the HDF5-file
 * (input file). This expands all datasets by one slab (in the 0th (time) dimension)
 * in the HDF5-file.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
herr_t write_timeslab(struct PHASE *p);

/** If datasets for chemical potentials, free-energy density or free energy exist in 
 * the input file (indicating that these should be saved), they are saved before the
 * simulation starts (OVERWRITING the last saved slab) without expanding the data-
 * sets in the HDF5-file.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
herr_t save_first_potentials(PHASE *p);

/** Close the HDF5 (input) file.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void close_file(struct PHASE *p);

/** Save the custom domain of a conversion to the input HDF5 file, after the custom
 * domain has been modified (when conversion zone follows interface during a snips simula-
 * tion).
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param cv Pointer to CONVERSION struct of the conversion whose custom domain has been 
 * modified
 */
herr_t write_custom_domain(struct PHASE *p, struct CONVERSION *cv);

#endif //SAVING_H
