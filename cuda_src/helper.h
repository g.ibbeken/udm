/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HELPER_H
#define HELPER_H

#include "phase.h"
/*---Function Declarations: ---------------------------------------------------------------------------------------------------------------------------------------------------*/
/** Calculate the average of a given array of size p->Nreal ON HOST.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param vec Pointer to array the average is calculated of, must be located on host.
 */
udm_scalar average(PHASE *p, udm_scalar *vec);

/** Calculate the maximum of the absolute value of a given array of size p->Nreal 
 * ON HOST.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param vec Pointer to array the average is calculated of, must be located on host.
 */
udm_scalar maxabs(PHASE *p, udm_scalar *vec);

/** For each component calculate the mean concentration and save in c->rho. 
 * This is DONE ON HOST and HOST MUST BE UPDATED beforehand!!
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param vec Pointer to array the average is calculated of, must be located on host.
 */
void reevaluate_averages(PHASE *p);

/** Calculate the root of the mean of the squares of a given array of size p->Nreal 
 * ON HOST.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param vec Pointer to array the average is calculated of, must be located on host.
 */
udm_scalar root_mean_square(PHASE *p, udm_scalar *vec);

/** Check if nan or inf value has appeared in array on device. 
 *
 * \param host Pointer to array on host that data can be copied to.
 * \param device Pointer to array on device which should be checked.
 * \param size Length of array.
 */
void nan_test(udm_scalar *host, udm_scalar *device, int size);

/** Copy an array between to host arrays
 *
 * \param array Pointer to array wich data is copied FROM.
 * \param target Pointer to array which data is copied TO.
 * \param size Length of array.
 */
void copy_array(udm_scalar *array, udm_scalar *target, int size);

/** This finds the interface position for evaporation simulations if ENABLE_EVAPORATION_
 * STABLITIZATION option is enabled. In the current version it enlargens the custom_array
 * domain in which conversions are performed, rather than shifting the fields (previous,
 * depricated version). This copies the concentration fields to host itself.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void find_solution_start(PHASE *p);

/** Shift the fields (for an evaporation simulation), as specified. For reflecting b.c., 
 * the first `shift` slabs are discared, the fields are shifted by this amont and the last
 * `shift` slabs are filled with the mean concentration of the initial conditions in the 
 * solution (saved in c->).
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param shift Number of slabs/grid cells (in first/z-direction) to shift.
 * \param copy Temporary array (on host) where to store data (size Nreal).
 */
void shift_fields(PHASE *p, int shift, udm_scalar *copy);

/** Give meaningfull error message.
 */
void HandleError(cudaError_t err, char const * const file, int const line);

/** Give meaningfull error message.
 */
void HandleError(cufftResult err, char const * const file, int const line);

/** Copy data from device to host (concentration fields (phi), chemical potentials (mu), 
 * and free-energy density (fe).
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void update_host(PHASE *p);

/** Helper function to print a field that is stored on the device. Only used for de-
 * bugging.
 *
 * \param host Pointer to array on host that data can be copied to.
 * \param device Pointer to array on device which should be checked.
 * \param n Number of elements to print.
 * \param name of the field to print at screen output.
 */
void print_field(udm_scalar *host, udm_scalar *device, int n, const char* name);

/** Helper function to print mean of an array on device. Only used for debugging.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param field Pointer to array on device which should be checked.
 * \param name of the field to print at screen output.
 */
void print_means(PHASE *p, udm_scalar *field, const char *name);

/** Wrapper function to find maximum of an array on deviced. This search is performed as a
 * reduction operation ON DEVICE. 
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param field Pointer to array on device which should be checked.
 * \param size Length of array.
 */
udm_scalar find_maximum(PHASE *p, udm_scalar *_field, int size);
#endif //HELPER_H
