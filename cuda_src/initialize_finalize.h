/* Copyright (c) 2020-2022 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INITIALIZE_FINALIZE_H
#define INITIALIZE_FINALIZE_H

#include "phase.h"

/** Call necessary functions to initial the full simulation. Afterwards, the PHASE struct 
 * is readily initialized.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param argc length of argument list of executable call
 * \param argv list of string pointers to argument list, first (index 1) contains input-
 * file name, second (optional) GPU id.
 */
void initialize_phase(PHASE *p, int argc, char **argv);

/** Finalize the simulation of it is done, including closing files and freeing memory.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void finalize_phase(PHASE *p);

/** Set up CUDA environment, specify device (GPU) id.
 *
 * \param gpu_number ID of the GPU to be set. If it is negative or larger than available,
 * last available device id is chosen.
 */
void setup_cuda(int gpu_number);

/** Allocate all necessary (device-) memory that has not yet been allocated at read-in.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void initialize_memory(PHASE *p);

/** Free all (device and host) memory.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void free_memory(PHASE *p);

/** Initialize parameters and parameter/helper fields and copy to device.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void init_parameters(PHASE *p);

/** Initialize cuFFT plans.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void init_fft(PHASE *p);

/** For regularization, generate a randomly permutated neighbor list and copy it to de-
 * vice. Saved in p->_neighbor_list.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void generate_random_neighbor_permutation(PHASE *p);

#endif //INITIALIZE_FINALIZE_H
