/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KERNELS_H
#define KERNELS_H

/*---Declarations: -------------------------------------------------------------------------------------------------------------------------------------------*/
/** Kernel to (element-wise) multiply two arrays and write result into separate array.
 *
 * \param result Pointer to array to which result is writen
 * \param factor0 Pointer to array with first factor
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the three above arrays
 */
__global__ void multiply_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal);

/** Kernel to (element-wise) multiply two arrays and write result into separate array, where the length of 
 * the second factor-array is smaller (typically Nrepeat=Nreal) but the length of the first two arrays is a multiple
 * of the length of the former (typically Ntotal=Nfull=n_components*Nreal).
 *
 * \param result Pointer to array to which result is writen, length Ntotal
 * \param factor0 Pointer to array with first factor, length Ntotal
 * \param factor1 Pointer to array with second factor, length Nrepeat
 * \param Ntotal Length of all the first two arrays
 * \param Nrepeat Length of all the last array. 
 */
__global__ void multiply_repeat_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal, int Nrepeat);

/** Kernel to (element-wise) multiply two arrays and write result in-place into first factor-array.
 *
 * \param factor0 Pointer to array with first factor, result strored in-place 
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the arrays
 */
__global__ void multiply_kernel1(udm_scalar *factor0, udm_scalar *factor1, int Ntotal);

/** Kernel to (element-wise) multiply a float/double array and a uint8_t-array and write result in-place 
 * into first factor-array.
 *
 * \param factor0 Pointer to array with first factor, result strored in-place 
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the arrays
 */
__global__ void multiply_kernel1_uint8(udm_scalar *factor0, uint8_t *factor1, int Ntotal);

/** Kernel to (element-wise) multiply two arrays and write result in-place into first array, where the length of 
 * the second factor-array is smaller (typically Nrepeat=Nreal) but the length of the first array is a multiple
 * of the length of the former (typically Ntotal=Nfull=n_components*Nreal).
 *
 * \param factor0 Pointer to array with first factor, length Ntotal, result stored in-place
 * \param factor1 Pointer to array with second factor, length Nrepeat
 * \param Ntotal Length of all the first two arrays
 * \param Nrepeat Length of all the last array. 
 */
__global__ void multiply_repeat_kernel1(udm_scalar *factor0, udm_scalar *factor1, int Ntotal, int Nrepeat);

/** Kernel to (element-wise) multiply two arrays (first complex, second real) and write result into separate 
 * (complex) array.
 *
 * \param result Pointer to array to which result is writen
 * \param factor0 Pointer to array with first factor
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the three above arrays
 */
__global__ void multiply_kernel_complex(fft_complex *result, fft_complex *factor0, fft_real *factor1, int Ntotal);

/** Kernel to (element-wise) multiply two arrays (first complex, second real) and write result into separate array, 
 * where the length of the second factor-array is smaller (typically Nrepeat=Ncomplex) but the length of the first 
 * two arrays is a multiple of the length of the former (typically Ntotal=n_components*Ncomplex).
 *
 * \param result Pointer to complex array to which result is writen, length Ntotal
 * \param factor0 Pointer to complex array with first factor, length Ntotal, 
 * \param factor1 Pointer to real array with second factor, length Nrepeat
 * \param Ntotal Length of all the first two arrays
 * \param Nrepeat Length of all the last array. 
 */
__global__ void multiply_repeat_kernel_complex(fft_complex *result, fft_complex *factor0, fft_real *factor1, int Ntotal, int Nrepeat);

/** Kernel to (element-wise) multiply arrays (complex) and scalar (real) and write result in-place into
 * first array.
 *
 * \param result Pointer to array to which result is writen
 * \param factor0 Pointer to array with first factor
 * \param factor1 Scalar (real), second factor
 * \param Ntotal Length of all the first two arrays
 */
__global__ void multiply_value_kernel1_complex(fft_complex *factor0, fft_real factor1, int Ntotal);

/** Kernel to (element-wise) multiply two complex arrays and write result in-place into first factor-array.
 *
 * \param factor0 Pointer to array with first factor, result strored in-place 
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the arrays
 */
__global__ void multiply_kernel1_complex(fft_complex *factor0, fft_real *factor1, int Ntotal);

/** Kernel to (element-wise) multiply two arrays (first complex, second real) and write result 
 * in-place into first array, where the length of the second factor-array is smaller 
 * (typically Nrepeat=Nreal) but the length of the first array is a multiple of the length of 
 * the former (typically Ntotal=Nfull=n_components*Nreal).
 *
 * \param factor0 Pointer to complex array with first factor, length Ntotal, result stored in-place
 * \param factor1 Pointer to real array with second factor, length Nrepeat
 * \param Ntotal Length of all the first array
 * \param Nrepeat Length of all the second array. 
 */
__global__ void multiply_repeat_kernel1_complex(fft_complex *factor0, fft_real *factor1, int Ntotal, int Nrepeat);

/** Kernel to (element-wise) add two arrays and write result into separate array.
 *
 * \param result Pointer to array to which result is writen
 * \param factor0 Pointer to array with first summand
 * \param factor1 Pointer to array with second summand
 * \param Ntotal Length of all the three above arrays
 */
__global__ void add_kernel(udm_scalar *result, udm_scalar *factor1, int Ntotal);

/** Kernel to (element-wise) multiply two arrays and ADD the result to separate array.
 *
 * \param result Pointer to array to which result is added
 * \param factor0 Pointer to array with first factor
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the three above arrays
 */
__global__ void add_and_multiply_kernel1(udm_scalar *result, udm_scalar factor0, udm_scalar *factor1, int Ntotal);

/** Kernel to (element-wise) multiply three arrays and ADD the result to separate array.
 *
 * \param result Pointer to array to which result is added
 * \param factor0 Pointer to array with first factor
 * \param factor1 Pointer to array with second factor
 * \param factor2 Pointer to array with third factor
 * \param Ntotal Length of all the four above arrays
 */
__global__ void add_and_multiply_kernel2(udm_scalar *result, udm_scalar factor0, udm_scalar *factor1, udm_scalar *factor2, int Ntotal);

/** Kernel to (element-wise) take the square root of an array and write result into separate array.
 *
 * \param result Pointer to array to which result is writen
 * \param argument Pointer to array which is argument to square root
 * \param Ntotal Length of all the two above arrays
 */
__global__ void sqrt_kernel(udm_scalar *result, udm_scalar *argument, int Ntotal);

/** Kernel to (element-wise) take the square root of the product of two arrays and write result 
 * into separate array.
 *
 * \param result Pointer to array to which result is writen
 * \param factor0 Pointer to array with first factor
 * \param factor1 Pointer to array with second factor
 * \param Ntotal Length of all the three above arrays
 */
__global__ void sqrt_of_product_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal);

/** Kernel to copy array 
 *
 * \param result Pointer to array TO which data is copied
 * \param argument Pointer to array FROM which data is copied
 * \param Ntotal Length of all the two above arrays
 */
__global__ void copy_kernel(udm_scalar *result, udm_scalar *argument, int Ntotal);

/** Kernel to copy array into an array of twice the size by concatenating 
 * the second half (mirrored first half of field-array) to the field-array.
 * This kernel creates the "full" field has periodic boundary conditions from a 
 * field that has reflecting boundary conditions (in first dimension) and makes 
 * it ready for the periodic cuFFT Fourier transform.
 *
 * \param result Pointer to array TO which data is copied, length 2*Ntotal
 * \param argument Pointer to array FROM which data is copied, length Ntotal
 * \param Ntotal Length of all the two above arrays
 */
__global__ void copy_symmetric_field_kernel(udm_scalar *full_field, udm_scalar *field, int Nz, int Ny, int Nx);

/** Calculates the deviation of the total (normalized) concentrations from 1 locally/element-wise.
 *
 * \param dphi Pointer to array in which result is stored
 * \param phi Pointer to array of all concentration fields, length n_components*Nreal
 * \param Nreal Length of dphi-array (Nx*Ny*Nz)
 * \param n_components Number of components
 */
__global__ void density_variation_kernel(udm_scalar *dphi, udm_scalar *phi, int Nreal, int n_components);

/** Calculates the deviation of the total (normalized) concentrations from 1 locally/element-wise, outside of a wall.
 * The latter is an array (wall) that is zero where there is no wall and 1 else. Hence it calculates the deviation
 * from (1-wall) element-wise.
 *
 * \param dphi Pointer to array in which result is stored
 * \param phi Pointer to array of all concentration fields, length n_components*Nreal
 * \param wall Pointer to array that specifies wall, length Nreal
 * \param Nreal Length of dphi-array (Nx*Ny*Nz)
 * \param n_components Number of components
 */
__global__ void density_variation_kernel(udm_scalar *dphi, udm_scalar *phi, uint8_t *wall, int Nreal, int n_components);

/** Calculate modified log-term that is continued to zero (where it vanishes).
 *          mu = factor * log(psi) if psi>0, else mu=0.
 *
 * \param mu Pointer to (component-specific) auxiliary chemical potentials tilde mu, length Nreal
 * \param psi Pointer to (component-specific) auxiliary field psi, length Nreal
 * \param Nreal Length of dphi-array (Nx*Ny*Nz)
 * \param factor prefactor to result
 */
__global__ void log_term_kernel(udm_scalar *mu, udm_scalar *psi, int Nreal, udm_scalar factor);

/** Calculate the contribution to auxiliary chemical potential that an umbrella field creates:
 *          mu += factor * psi * (phi-umbrella_field), 
 * where phi = psi*psi and factor contains the factor 2 that comes from calculating aux chem. pot. from 
 * regular chem pot.
 *
 * \param mu Pointer to (component-specific) auxiliary chemical potentials tilde mu, length Nreal
 * \param psi Pointer to (component-specific) auxiliary field psi, length Nreal
 * \param umbrealla Pointer to (component-specific) target concentration phi_0, length Nreal
 * \param Nreal Length of dphi-array (Nx*Ny*Nz)
 * \param factor prefactor to result
 */
__global__ void umbrella_contribution_kernel(udm_scalar *mu, udm_scalar *psi, udm_scalar *umbrella_field, int Nfull, udm_scalar factor);

/** Calculate the thermal noise contribution given newly generated unit-var, zero-mean random numbers. 
 * Calculation of eq. (22) in model_introduction.pdf.
 *
 * \param to_add Pointer to which result is added (explicit increment)
 * \param factor prefactor to result, = sqrt(lambda_c/lambda_0/sqrtNbar/2)
 * \param psi Pointer to auxiliary field psi 
 * \param noise Pointer to (readily generated) random numbers  
 * \param oodz32 Scalar that contains the factor 1/dz/sqrt(dxdydz)
 * \param oody32 Scalar that contains the factor 1/dy/sqrt(dxdydz)
 * \param oodx32 Scalar that contains the factor 1/dx/sqrt(dxdydz)
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 */
__global__ void add_noise_term_kernel(udm_scalar *to_add, udm_scalar factor, udm_scalar *psi, udm_scalar *noise, udm_scalar oodz32, udm_scalar oody32, udm_scalar oodx32, int Nz, int Ny, int Nx);

/** Kernel that checks if concentrations are smaller zero and if this is the case, calls kernel to accumulate the
 * necessary material from the neighbors.
 *
 * \param phi Pointer to concentration fields (all), length Nfull
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param Nfull Length of phi-array (n_components*Nx*Ny*Nz)
 * \param neighbor_list Pointer to permutated neighbor list that has already been created
 */
__global__ void regularize_smaller_zero_kernel(udm_scalar *phi, int Nz, int Ny, int Nx, int Nfull, int *neighbor_list);

/** Get the flattened index, given z-, y-, and x-indices.
 *
 * \param z Index in first/z-dimension
 * \param y Index in second/y-dimension
 * \param x Index in third/x-dimension
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 */
__device__ __host__ unsigned int index_from_indices(int z, int y, int x, int Nz, int Ny, int Nx);

/** Get the indices in each dimension, given the flattened index.
 *
 * \param z Flattened (1d) index
 * \param z Pointer to index in first/z-dimension
 * \param y Pointer to index in second/y-dimension
 * \param x Pointer to index in third/x-dimension
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 */
__device__ __host__ void indices_from_index(unsigned int index, int *z, int *y, int *x, int Nz, int Ny, int Nx);

/** Given a grid cell, where concentration is <0, choose random neighbors and check whether these have available material
 * to get the ceoncentration to zero. This implements the regularization described in point 6 in model_introduction.pdf.
 * The random neighbor generation now takes the random neighbor list (length 729*3, i.e. three neighbor indices dz, dy, dx 
 * for the 729 (NUM_NEIGHBORS_TOTAL) total neighbors) and gets a random index in this list where it starts trying. To get 
 * this random starting point, this method casts the (small) absolute value of the negative grid cell as an integer. Its 
 * remainder (modulo) within NUM_NEIGHBORS_TOTAL is the start value. This new method also ensures that after NUM_NEIGHBORS_
 * TOTAL tries all neighbors have been checked.
 * 
 * WARNING: Must be sure that the concentration 
 *
 * \param phi Pointer to concentration field (single), length Nz*Ny*Nx
 * \param i Index at which negative concentration has been detected.
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param neighbor_list Pointer to permutated neighbor list that has already been created
 */
__device__ void take_from_neighbors(udm_scalar *phi, int i, int Nz, int Ny, int Nx, int *neighbor_list);

/** Write scalar value into an array.
 *
 * \param field Array to intialize with value.
 * \param value Scalar value that is written to array.
 * \param Nreal Length of array (Nx*Ny*Nz)
 */
__global__ void initialize_value_kernel(udm_scalar *field, udm_scalar value, int Nreal);

/** Write scalar uint8_t-value into an uint8_t-array.
 *
 * \param field Array to intialize with value
 * \param value Scalar value that is written to array.
 * \param Nreal Length of array (Nx*Ny*Nz)
 */
__global__ void initialize_value_kernel(uint8_t *field, uint8_t value, int Nreal);

/** Free-energy density of a (given its index) homopolymer that is added to free-energy density array. 
 *
 * \param fe Pointer to array that stores free-energy density
 * \param phi Pointer to concentration fields (all), length n_components*Nz*Ny*Nx
 * \param lap Pointer to laplacian of the concentration fields (all)
 * \param ci Index of the given component (which is homopolymer)
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param dz Spatial discretization in first/z-dimension
 * \param dy Spatial discretization in second/y-dimension
 * \param dx Spatial discretization in third/x-dimension
 * \param n_components Number of components
 * \param param0 prefactor to log-term
 * \param param1 prefactor to square-gradient-term
 * \param chiN Pointer to interaction array (stored on device)
 */
__global__ void add_homopolymer_contribution_free_energy_kernel(udm_scalar *fe, udm_scalar *phi, udm_scalar *lap, int ci, int Nz, int Ny, int Nx, udm_scalar dz, udm_scalar dy, udm_scalar dx, int n_components, udm_scalar param0, udm_scalar param1, udm_scalar *chiN);

/** Free-energy density of a (given its index) diblock copolymer block that is added to free-energy density array. 
 *
 * \param fe Pointer to array that stores free-energy density
 * \param phi Pointer to concentration fields (all), length n_components*Nz*Ny*Nx
 * \param conv Pointer to (readily calc.) convolutions of psi with long-range kernel (all)
 * \param lap Pointer to  (readily calc.) laplacian of the concentration fields (all)
 * \param ci Index of the given component (which is a block of a diblock copolymer)
 * \param cibonded Index of the other component in the diblock copolymer 
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param dz Spatial discretization in first/z-dimension
 * \param dy Spatial discretization in second/y-dimension
 * \param dx Spatial discretization in third/x-dimension
 * \param n_components Number of components
 * \param param0 prefactor to log-term
 * \param param1 prefactor to first convolution 
 * \param param2 prefactor to second convolution multiplied with psi of bonded comp
 * \param param3 prefactor to fraction of sqrts of psi
 * \param param4 prefactor to square-gradient term
 * \param chiN Pointer to interaction array (stored on device)
 */
__global__ void add_diblock_copolymer_contribution_free_energy_kernel(udm_scalar *fe, udm_scalar *phi, udm_scalar *conv, udm_scalar *lap, int ci, int cibonded, int Nz, int Ny, int Nx, udm_scalar dz, udm_scalar dy, udm_scalar dx, int n_components, udm_scalar param0, udm_scalar param1, udm_scalar param2, udm_scalar param3, udm_scalar param4, udm_scalar *chiN);

/** Calculate laplacian (3D) via finite differences.
 *
 * \param laplacian Pointer to array to which result (Laplacian of field) is writen
 * \param field Pointer to array which Laplacian is calculated for
 * \param Nreal Length of field array (Nx*Ny*Nz)
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param oodz2 Scalar that contains the factor 1/dz/dz
 * \param oody2 Scalar that contains the factor 1/dy/dy
 * \param oodx2 Scalar that contains the factor 1/dx/dx
 */
__global__ void compute_laplacian_kernel(udm_scalar *laplacian, udm_scalar *field, int Nreal, int Nz, int Ny, int Nx, udm_scalar oodz2, udm_scalar oody2, udm_scalar oodx2);

/** Compute the explicit increment (without noise term) in the finite difference version that allows spatially dependent mobility
 *
 * \param field Pointer to array in which result is stored.
 * \param factor Prefactor to calculation, = dt/4.
 * \param mobility Pointer to mobility-array that is readily evaluated
 * \param psi Pointer to sqrt of concentration fields psi (single)
 * \param Nreal Length of field array (Nx*Ny*Nz)
 * \param Nz Number of grid cells in first/z-dimension
 * \param Ny Number of grid cells in second/y-dimension
 * \param Nx Number of grid cells in third/x-dimension
 * \param oodz2 Scalar that contains the factor 1/dz/dz
 * \param oody2 Scalar that contains the factor 1/dy/dy
 * \param oodx2 Scalar that contains the factor 1/dx/dx
 */
__global__ void compute_dtgamma_kernel(udm_scalar *field, udm_scalar factor, udm_scalar *mobility, udm_scalar *psi, udm_scalar *mu, int Nreal, int Nz, int Ny, int Nx, udm_scalar oodz2, udm_scalar oody2, udm_scalar oodx2);

/** Function to screen-print a field on device, only here for debugging, not actually used.
 *
 * \param field Pointer to array that is printed
 * \param Nfull Length of field array
 */
__global__ void print_field_from_device(udm_scalar *field, int Nfull);

/** Kernel that finds maximum of an array ON DEVICE, i.e. implements reduction operation.
 *
 * \param minimum Pointer to scalar float/double where result is stored
 * \param field Pointer to array for which the minimum is found
 * \param Nfull Length of field array
 */
__global__ void find_maximum_kernel(udm_scalar *minimum, udm_scalar *field, int Nfull);

/** Calculate the density-dependent mobility and write to dedicated array (on device).
 *
 * \param mobility Pointer to mobility-array in which the result is stored
 * \param factor Prefactor to calculation, = lambda_c/lamdba_0/2.
 * \param ddm_parameters Pointer to parameters for calculation of density-dependent mobility (as stored in COMPONENT struct)
 * \param phi Pointer to concentration fields (all), length n_components * Nreal
 * \param n_components Number of components
 * \param Nreal Length of mobility array (Nx*Ny*Nz)
 */
__global__ void calculate_density_dependent_mobilty_kernel(udm_scalar *mobility, udm_scalar factor, udm_scalar *ddm_parameters, udm_scalar *phi, int n_components, int Nreal);


#endif //KERNELS_H
