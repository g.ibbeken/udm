/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <time.h>
#include <curand.h>
#include <curand_kernel.h>

#include "random.h"
#include "phase.h"
#include "helper.h"
#include "kernels.h"

__global__ void setup_rand_states_kernel(curandState_t *rand_states, int Ntotal)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        curand_init(1234, i, 0, &rand_states[i]);
}

void init_random_states(PHASE *p)
{
    HANDLE_ERROR(cudaMalloc((void **)&p->_rand_states, (3*p->Nfull/2)*sizeof(curandState_t))); 
    dim3 blocks ((3*p->Nfull/2 + p->threads.x - 1)/p->threads.x, 1, 1);
    setup_rand_states_kernel<<<blocks, p->threads>>>(p->_rand_states, 3*p->Nfull/2);
}

__global__ void renew_noise_kernel(double *noise, curandState_t *rand_states, int Ntotal)
{
    int i = 2 * (blockIdx.x * blockDim.x + threadIdx.x);
    if( (i+1) < Ntotal )
    {
        double2 two_normal_numbers = curand_normal2_double(rand_states+i/2);
        noise[i] = two_normal_numbers.x;
        noise[i+1] = two_normal_numbers.y;
        
        //the following code was box muller transform using curand_uniform,
        //now use normal number generator directly.

        //curandState_t *rstate = rand_states + i;
        //double u1, u2, r;
        //do{
        //u1 = 2 * curand_uniform(rstate) - 1.;
        //u2 = 2 * curand_uniform(rstate) - 1.;
        //r = u1 * u1 + u2 * u2;
        //} while (r > 1);
        //const double mult = sqrt (-2. * log (r) / r);

        //noise[i]   = mult * u1;
        ////noise[i+1] = mult * u2; //we could potentially double performance by also using this.
    }
}

__global__ void renew_noise_kernel(float *noise, curandState_t *rand_states, int Ntotal) //declaration for float and double to cover both cases (decided at compile time)
{
    int i = 2 * ( blockIdx.x * blockDim.x + threadIdx.x );
    if( (i+1) < Ntotal )
    {
        float2 two_normal_numbers = curand_normal2(rand_states+i/2);
        noise[i] = two_normal_numbers.x;
        noise[i+1] = two_normal_numbers.y;
        
        //the following code was box muller transform using curand_uniform,
        //now use normal number generator directly.

        //curandState_t *rstate = rand_states + i;
        //float u1, u2, r;
        //do{
        //u1 = 2 * curand_uniform(rstate) - 1.;
        //u2 = 2 * curand_uniform(rstate) - 1.;
        //r = u1 * u1 + u2 * u2;
        //} while (r > 1);
        //const float mult = sqrt (-2. * log (r) / r);

        //noise[i]   = mult * u1;
        ////noise[i+1] = mult * u2; //we could potentially float performance by also using this.
    }
}

void add_thermal_noise(PHASE *p)
{
#ifdef ENABLE_NOISE
    //regenerate zero-mean, unit variance fields 
    dim3 blocks ((3*p->Nfull/2 + p->threads.x - 1 )/p->threads.x, 1, 1);
    renew_noise_kernel<<<blocks, p->threads>>>(p->_noise, p->_rand_states, 3*p->Nfull);
    //precalculcation for ddm
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    udm_scalar *_sqrtlambdaphi = p->_lap_psi;
    sqrt_of_product_kernel<<<p->blocks_multiple, p->threads>>>(_sqrtlambdaphi, p->_mobility, p->_phi, p->Nfull); 
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY

    //now add noise to dtgamma (aka _conv_psi - array).
    for(int ci =0; ci<p->n_components; ci++)
    {
        COMPONENT *c = p->comps[ci];
        udm_scalar *_dtgamma = c->_conv_psi;

#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
        const udm_scalar factor = p->noise_prefactor * sqrt(c->m);
        add_noise_term_kernel<<<p->blocks_single, p->threads>>>(_dtgamma, factor, c->_psi, c->_noise, p->oodz32, p->oody32, p->oodx32, p->N[0], p->N[1], p->N[2]);
#else //IF ENABLE_DENSITY_DEPENDENT_MOBILITY
        _sqrtlambdaphi = c->_lap_psi;
        add_noise_term_kernel<<<p->blocks_single, p->threads>>>(_dtgamma, p->noise_prefactor, _sqrtlambdaphi, c->_noise, p->oodz32, p->oody32, p->oodx32, p->N[0], p->N[1], p->N[2]);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY

    }
#endif //ENABLE_NOISE
}

