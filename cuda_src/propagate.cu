/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
//#include <mkl.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <hdf5.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <curand.h>
#include <curand_kernel.h>

#include "propagate.h"
#include "phase.h"
#include "helper.h"
#include "saving.h"
#include "random.h"
#include "kernels.h"

#ifndef M_PI
#define M_PI
#define PI 3.14159265
#endif 

#ifdef ENABLE_SINGLE_PRECISION
#define CUFFT_EXECUTER_R2C cufftExecR2C
#define CUFFT_EXECUTER_C2R cufftExecC2R
#else //ENABLE_SINGLE_PRECISION
#define CUFFT_EXECUTER_R2C cufftExecD2Z
#define CUFFT_EXECUTER_C2R cufftExecZ2D
#endif //ENABLE_SINGLE_PRECISION


/*---Functions: -------------------------------------------------------------------------------------------------------------------------------------------*/
int simulate(PHASE *p)
{
    //return variable:
    int return_val=0;
    printf("Starting simulation... \n");
    fflush(stdout);

    //calculate chemical potential and free energy of last time slab and save.
    evaluate_free_energy(p);
    save_first_potentials(p);

    //for Gauss-Seidel method need delta_phi (saved in lap_mu) to be init 0
    initialize_value_kernel<<<p->blocks_multiple, p->threads>>>(p->_lap_mu, 0., p->Nfull);

    //try out fft: 
//#ifdef ENABLE_REFLECTING_BC
//    copy_symmetric_field_kernel<<<p->blocks_single, p->threads>>>(p->_fields_full, p->_phi, p->N[0], p->N[1], p->N[2]);
//    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_single_forward, p->_fields_full, p->_fphi));
//    udm_scalar Nlogical = (2*p->N[0]-2)*p->N[1]*p->N[2];
//    multiply_value_kernel1_complex<<<p->blocks_single_fs, p->threads>>>(p->_fphi, 1/Nlogical, p->Ncomplex);
//    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, p->_fphi, p->_fields_full));
//    copy_kernel<<<p->blocks_single, p->threads>>>(p->_mu, p->_fields_full, p->Nreal);
//    update_host(p);
//    printf("\n\nTEST CONCENTRATION: \n");
//    for(int i=0; i<p->Nreal; i++)
//    {
//        if( p->phi[i] != p->mu[i] )
//        {
//            printf("Difference in density detected: %.10e %.10e %e\n", p->phi[i], p->mu[i], p->phi[i]-p->mu[i]);
//        }
//    }
//#endif //ENABLE_REFLECTING_BC
    
    //timing:
    struct timeval start_t, now_t;
    gettimeofday(&start_t, NULL);

    //===== Time stepping. ==================================================================================================================================================
    for(long long t=llround(p->Tstart/p->dt)+(long long)1;t<=p->Nt;t++)
    { 
        time_step(p);
        if(t%p->tsave==0)
        {
            evaluate_free_energy(p); //this updates host.
            reevaluate_averages(p);
            int save_state = write_timeslab(p);
            if(save_state==-1){
                printf("ERROR: during saving at t=%f, slab %lld. Exiting...\n", t*p->dt,t/p->tsave);
                return_val=3;
                break;
            } else if (save_state==-2){
                return_val=2;
                break;
            } else {
                gettimeofday(&now_t, NULL);
                const double diff_secs = now_t.tv_sec - start_t.tv_sec + (now_t.tv_usec - start_t.tv_usec)*1e-6;
                const double tps = p->tsave/diff_secs;
                const time_t end_time = now_t.tv_sec + (p->Nt + 1 - t)/tps;

                printf("t=%.1f, averages:", t*p->dt);
                for(int cn=0; cn<p->n_components; cn++)
                {
                    printf("%d: %.3f ", cn, p->comps[cn]->rho);  
                }
                printf("F=%.3f, ", p->Fe);
#ifdef ENABLE_EVAPORATION_STABILIZATION
                printf("z_interface=%.1f, ", (p->shift+p->interface_position)*p->dr[0]);
#endif //ENABLE_EVAPORATION_STABILIZATION
                printf("TPS = %.1f, ETA: %s", tps, ctime(&end_time));
                gettimeofday(&start_t, NULL);
                //printf("its: %d, F=%e.\n", iterations, p->Fe);
                fflush(stdout);
            }
        }
    }
    //==============================================================================================================================================================================
    return(return_val);
}

void time_step(PHASE *p)
{
    evaluate_mu(p);
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    evaluate_mobility(p);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    evaluate_explicit_increment(p);
    evaluate_implicit_method(p);
    regularize_phi(p);
#ifdef ENABLE_EVAPORATION_STABILIZATION
    static int i = 0;
    if( (++i)%100 == 0 && p->conversion_delay > 0)
    {
        find_solution_start(p);
        initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->_phi, 1e-8, (p->interface_position-8)*p->Nslice);
        initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->_phi+p->Nreal, 1e-8, (p->interface_position-8)*p->Nslice);
#ifndef ENABLE_REFLECTING_BC
        initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->_phi+p->Nreal-(p->interface_position-8)*p->Nslice, 1e-8, (p->interface_position-8)*p->Nslice);
        initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->_phi+2*p->Nreal-(p->interface_position-8)*p->Nslice, 1e-8, (p->interface_position-8)*p->Nslice);
#endif //ENABLE_REFLECTING_BC
    }
#endif //ENABLE_EVAPORATION_STABILIZATION
#ifdef ENABLE_CONVERSIONS
    perform_conversions(p);
#endif //ENABLE_CONVERSIONS
}

void evaluate_mu(PHASE *p)
{
    precalculation(p);
    for(int ci=0; ci<p->n_components; ci++)
    {
        switch(p->comps[ci]->arch){
            case P1:
                evaluate_mu_homopolymer(p, p->comps[ci]);
                break;
            case P2:
                evaluate_mu_diblock(p, p->comps[ci]);
                break;
            default:;
        }
    }
#ifdef ENABLE_EXTERNAL_FIELD
    if(p->external_field != NULL)
        add_external_field_contribution(p);
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if(p->umbrella_field != NULL)
        add_umbrella_field_contribution(p);
#endif //ENABLE_EXTERNAL_FIELD
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
    evaluate_laplace_mu(p);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
}

void precalculation(PHASE *p)
{
    //dphi
    calculate_density_variation(p);

    //lap_psi & conv_psi
    sqrt_kernel<<<p->blocks_multiple, p->threads>>>(p->_psi, p->_phi, p->Nfull);
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
    copy_kernel<<<p->blocks_multiple, p->threads>>>(p->_lap_psi, p->_psi, p->Nfull);
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_lap_psi, p->_fpsi));
    for( unsigned int ic=0; ic<p->n_components; ic++)
    {
        COMPONENT *c = p->comps[ic];
        if( c->arch == P2 )
        {
            multiply_kernel_complex<<<p->blocks_single_fs, p->threads>>>(c->_fconv, c->_fpsi, p->_Kern, p->Ncomplex);
            HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, c->_fconv, c->_conv_psi));
        }
    }
    multiply_repeat_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fpsi, p->_K2, p->n_components*p->Ncomplex, p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fpsi, p->_lap_psi));
#else //IF ENABLE_DENSITY_DEPENDENT_MOBILITY
    //copy_kernel<<<p->blocks_multiple, p->threads>>>(p->_lap_psi, p->_psi, p->Nfull);
    //HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_lap_psi, p->_fpsi));
    //multiply_repeat_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fpsi, p->_K2, p->n_components*p->Ncomplex, p->Ncomplex);
    //HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fpsi, p->_lap_psi));
    for( unsigned int ic=0; ic<p->n_components; ic++)
    {
        COMPONENT *c = p->comps[ic];
        compute_laplacian_kernel<<<p->blocks_single, p->threads>>>(c->_lap_psi, c->_psi, p->Nreal, p->N[0], p->N[1], p->N[2], p->oodz2, p->oody2, p->oodx2);
        if( c->arch == P2 )
        {
#ifndef ENABLE_REFLECTING_BC
            copy_kernel<<<p->blocks_single, p->threads>>>(c->_conv_psi, c->_psi, p->Nreal);
            HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_single_forward, c->_conv_psi, c->_fconv));
            multiply_kernel1_complex<<<p->blocks_single_fs, p->threads>>>(c->_fconv, p->_Kern, p->Ncomplex);
            HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, c->_fconv, c->_conv_psi));
#else //IF ENABLE_REFLECTING_BC
            copy_symmetric_field_kernel<<<p->blocks_single, p->threads>>>(c->_field_full, c->_psi, p->N[0], p->N[1], p->N[2]);
            HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_single_forward, c->_field_full, c->_fphi));
            multiply_kernel1_complex<<<p->blocks_single_fs, p->threads>>>(c->_fphi, p->_Kern, p->Ncomplex);
            HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, c->_fphi, c->_field_full));
            copy_kernel<<<p->blocks_single, p->threads>>>(c->_conv_psi, c->_field_full, p->Nreal);
#endif //ENABLE_REFLECTING_BC
        }
    }
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
}

void calculate_density_variation(PHASE *p)
{
#ifdef ENABLE_EXTERNAL_FIELD
    if( p->wall != NULL )
        density_variation_kernel<<<p->blocks_single, p->threads>>>(p->_dphi, p->_phi, p->_wall, p->Nreal, p->n_components);
    else
        density_variation_kernel<<<p->blocks_single, p->threads>>>(p->_dphi, p->_phi, p->Nreal, p->n_components);
#else //ENABLE_EXTERNAL_FIELD
    density_variation_kernel<<<p->blocks_single, p->threads>>>(p->_dphi, p->_phi, p->Nreal, p->n_components);
#endif //ENABLE_EXTERNAL_FIELD
#ifndef ENABLE_REFLECTING_BC
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_single_forward, p->_dphi, p->_fdphi));
    multiply_kernel1_complex<<<p->blocks_single_fs, p->threads>>>(p->_fdphi, p->_Kern, p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, p->_fdphi, p->_dphi));
#else //IF ENABLE_REFLECTING_BC
    copy_symmetric_field_kernel<<<p->blocks_single, p->threads>>>(p->_fields_full, p->_dphi, p->N[0], p->N[1], p->N[2]);
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_single_forward, p->_fields_full, p->_fdphi));
    multiply_kernel1_complex<<<p->blocks_single_fs, p->threads>>>(p->_fdphi, p->_Kern, p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_single_backward, p->_fdphi, p->_fields_full));
    copy_kernel<<<p->blocks_single, p->threads>>>(p->_dphi, p->_fields_full, p->Nreal);
#endif //ENABLE_REFLECTING_BC
}

void evaluate_mu_diblock(PHASE *p, COMPONENT *c)
{
    COMPONENT *cbonded = p->comps[c->bonded_comp_index];
    log_term_kernel<<<p->blocks_single, p->threads>>>(c->_mu, c->_psi, p->Nreal, c->params[0]);
    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(c->_mu, c->params[1], c->_conv_psi, p->Nreal);
    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(c->_mu, c->params[2], cbonded->_conv_psi, p->Nreal);
    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(c->_mu, c->params[3], cbonded->_psi, p->Nreal);
    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(c->_mu, c->params[4], c->_lap_psi, p->Nreal);
    add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(c->_mu, 1/p->dt, c->_psi, p->_dphi, p->Nreal);
    for(int j=0; j<p->n_components; j++)
    {
        add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(c->_mu, 2*p->chiN[c->index*p->n_components + j], c->_psi, p->comps[j]->_phi, p->Nreal);
    }
}
        
void evaluate_mu_homopolymer(PHASE *p, COMPONENT *c)
{
    log_term_kernel<<<p->blocks_single, p->threads>>>(c->_mu, c->_psi, p->Nreal, c->params[0]);
    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(c->_mu, c->params[1], c->_lap_psi, p->Nreal);
    add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(c->_mu, 1/p->dt, c->_psi, p->_dphi, p->Nreal);
    for(int j=0; j<p->n_components; j++)
    {
        add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(c->_mu, 2*p->chiN[c->index*p->n_components + j], c->_psi, p->comps[j]->_phi, p->Nreal);
    }
}

void add_external_field_contribution(PHASE *p)
{ 
#ifdef ENABLE_EXTERNAL_FIELD
    add_and_multiply_kernel2<<<p->blocks_multiple, p->threads>>>(p->_mu, 2. * p->comps[0]->N, p->_psi, p->_external_field, p->Nfull);
#endif //ENABLE_EXTERNAL_FIELD
}

void add_umbrella_field_contribution(PHASE *p)
{ 
#ifdef ENABLE_UMBRELLA_FIELD
    umbrella_contribution_kernel<<<p->blocks_multiple, p->threads>>>(p->_mu,  p->_psi, p->_umbrella_field, p->Nfull, 2.*p->umbrella_kappa * p->comps[0]->N);
#endif //ENABLE_UMBRELLA_FIELD
}

void evaluate_laplace_mu(PHASE *p)
{
    copy_kernel<<<p->blocks_multiple, p->threads>>>(p->_lap_mu, p->_mu, p->Nfull);
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_lap_mu, p->_fmu));
    multiply_repeat_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fmu, p->_K2, p->n_components*p->Ncomplex, p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fmu, p->_lap_mu));
}

void evaluate_explicit_increment(PHASE *p)
{
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
    initialize_value_kernel<<<p->blocks_multiple, p->threads>>>(p->_conv_psi, 0., p->Nfull); //we use the array conv_psi to evaluate "dtgamma"
    for(int ci =0; ci<p->n_components; ci++)
    {
        COMPONENT *c = p->comps[ci];
        udm_scalar factor = p->dt * c->m/2.;
        udm_scalar *_dtgamma = c->_conv_psi;

        add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(_dtgamma, factor, c->_psi, c->_lap_mu, p->Nreal);
        add_and_multiply_kernel2<<<p->blocks_single, p->threads>>>(_dtgamma, -factor, c->_lap_psi, c->_mu, p->Nreal);
    }
#else //if ENABLE_DENSITY_DEPENDENT_MOBILITY
    for(int ci =0; ci<p->n_components; ci++)
    {
        COMPONENT *c = p->comps[ci];
        udm_scalar factor = p->dt / 4.;
        udm_scalar *_dtgamma = c->_conv_psi;

        compute_dtgamma_kernel<<<p->blocks_single, p->threads>>>(_dtgamma, factor, c->_mobility, c->_psi, c->_mu, p->Nreal, p->N[0], p->N[1], p->N[2], p->oodz2, p->oody2, p->oodx2);
    }
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    add_thermal_noise(p);
}

void evaluate_implicit_method(PHASE *p)
{
#ifndef ENABLE_REFLECTING_BC
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_conv_psi, p->_fphi)); //dtgamma was saved in conv_psi
    multiply_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fphi, p->_Lin, p->n_components*p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fphi, p->_conv_psi));
    add_kernel<<<p->blocks_multiple, p->threads>>>(p->_phi, p->_conv_psi, p->Nfull);
#else //IF ENABLE_REFLECTING_BC
    for(int ci=0; ci<p->n_components; ci++)
    {
        COMPONENT *c = p->comps[ci];
        copy_symmetric_field_kernel<<<p->blocks_single, p->threads>>>(c->_field_full, c->_conv_psi, p->N[0], p->N[1], p->N[2]);

    }
    HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_fields_full, p->_fphi)); //dtgamma was saved in conv_psi
    multiply_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fphi, p->_Lin, p->n_components*p->Ncomplex);
    HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fphi, p->_fields_full));
    for(int ci=0; ci<p->n_components; ci++)
    {
        COMPONENT *c = p->comps[ci];
        add_kernel<<<p->blocks_single, p->threads>>>(c->_phi, c->_field_full, p->Nreal);

    }
#endif //ENABLE_REFLECTING_BC
/*#else //if ENABLE_DENSITY_DEPENDENT_MOBILITY
    static int adi_index = 0;
    const unsigned int iterations_before_check = 20;
    for(int ic=0; ic<p->n_components; ic++)
    {
        COMPONENT *c = p->comps[ic];
        udm_scalar error = 1;
        udm_scalar factor;
        udm_scalar *_delta_phi = c->_lap_mu;
        udm_scalar *_dtgamma = c->_conv_psi;
        udm_scalar *_difference = c->_lap_psi;

        unsigned int iterations = 0;
        while( error > 1e-5 )
        {
            //printf("direction: %d \n", adi_index);
            switch(adi_index)
            {
                case 0:
                    factor = p->dt*p->oodz2*p->oodz2/24.;
                    for(int iteration=0; iteration<iterations_before_check; iteration++)
                        gauss_seidel_iteration_kernel_z<<<p->blocks_single, p->threads>>>(_delta_phi, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //gauss_seidel_iteration_with_difference_kernel_z<<<p->blocks_single, p->threads>>>(_delta_phi, _difference, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //error = find_maximum(p, _difference, p->Nreal);
                    break;
                case 1:
                    factor = p->dt*p->oody2*p->oody2/24.;
                    for(int iteration=0; iteration<iterations_before_check; iteration++)
                        gauss_seidel_iteration_kernel_y<<<p->blocks_single, p->threads>>>(_delta_phi, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //gauss_seidel_iteration_with_difference_kernel_y<<<p->blocks_single, p->threads>>>(_delta_phi, _difference, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //error = find_maximum(p, _difference, p->Nreal);
                    break;
                case 2:
                    factor = p->dt*p->oodx2*p->oodx2/24.;
                    for(int iteration=0; iteration<iterations_before_check; iteration++)
                        gauss_seidel_iteration_kernel_x<<<p->blocks_single, p->threads>>>(_delta_phi, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //gauss_seidel_iteration_with_difference_kernel_x<<<p->blocks_single, p->threads>>>(_delta_phi, _difference, _dtgamma, c->_mobility, factor, p->Nreal, p->N[0], p->N[1], p->N[2]);
                    //error = find_maximum(p, _difference, p->Nreal);
                    break;
                default:
                    error = 0;
                    break;
            }
            error = 0;//!!! instant exit

            iterations += iterations_before_check + 1;
            //printf("%e ", error);
        }
        p->gs_iterations = iterations;
        //printf("\n iterations: %d\n", iterations);
    }
    adi_index = (adi_index+1) % p->dim;
    //HANDLE_ERROR(CUFFT_EXECUTER_R2C(p->plan_multiple_forward, p->_lap_mu, p->_fphi)); //delta_phi was saved in lap_mu
    //multiply_value_kernel1_complex<<<p->blocks_multiple_fs, p->threads>>>(p->_fphi, 1/(udm_scalar)p->Nreal, p->n_components*p->Ncomplex);
    //vanish_rho<<<1, p->n_components>>>(p->_fphi, p->Ncomplex, p->Nreal);
    //HANDLE_ERROR(CUFFT_EXECUTER_C2R(p->plan_multiple_backward, p->_fphi, p->_lap_mu));
    add_kernel<<<p->blocks_multiple, p->threads>>>(p->_phi, p->_lap_mu, p->Nfull);
    //add_kernel<<<p->blocks_multiple, p->threads>>>(p->_phi, p->_conv_psi, p->Nfull);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY*/
}

void regularize_phi(PHASE *p)
{
    regularize_smaller_zero_kernel<<<p->blocks_multiple, p->threads>>>(p->_phi, p->N[0], p->N[1], p->N[2], p->Nfull, p->_neighbor_list); 
}

void perform_conversions(PHASE *p)
{   //perform conversions by going through each one 
    for(int conv_i=0; conv_i<p->n_conversions; conv_i++)
    {   //first calculate buffers
        CONVERSION *conv = p->convs[conv_i];
        udm_scalar *_buffer = p->_conv_psi + conv_i * p->Nreal;

        switch(conv->type)
        {
            case global:
                initialize_value_kernel<<<p->blocks_single, p->threads>>>(_buffer, conv->factor, p->Nreal);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_single, p->threads>>>(_buffer, conv->reactants[react_i]->_phi, p->Nreal);
                break;
            case custom:
                initialize_value_kernel<<<p->blocks_single, p->threads>>>(_buffer, conv->factor, p->Nreal);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_single, p->threads>>>(_buffer, conv->reactants[react_i]->_phi, p->Nreal);
                multiply_kernel1_uint8<<<p->blocks_single, p->threads>>>(_buffer, conv->_custom_domain, p->Nreal);
                break;
            case local_left:;
                initialize_value_kernel<<<p->blocks_slice, p->threads>>>(_buffer, conv->factor, p->Nslice);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_slice, p->threads>>>(_buffer, conv->reactants[react_i]->_phi, p->Nslice);
                break;
            case local_right:;
                initialize_value_kernel<<<p->blocks_slice, p->threads>>>(_buffer, conv->factor, p->Nslice);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_slice, p->threads>>>(_buffer, conv->reactants[react_i]->_phi+p->Nreal-p->Nslice, p->Nslice);
                break;
            case transport_left:;
                initialize_value_kernel<<<p->blocks_slice, p->threads>>>(_buffer, conv->factor, p->Nslice);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_slice, p->threads>>>(_buffer, conv->reactants[react_i]->_phi+p->Nreal/2-p->Nslice, p->Nslice);
                break;
            case transport_right:;
                initialize_value_kernel<<<p->blocks_slice, p->threads>>>(_buffer, conv->factor, p->Nslice);
                for(int react_i=0; react_i<conv->n_reactants; react_i++)
                    multiply_kernel1<<<p->blocks_slice, p->threads>>>(_buffer, conv->reactants[react_i]->_phi+p->Nreal/2, p->Nslice);
                break;
            default:;
        }
    }
    for(int conv_i=0; conv_i<p->n_conversions; conv_i++)
    {   //Secondly, perform conversion with the buffer for each one
        CONVERSION *conv = p->convs[conv_i];
        udm_scalar *_buffer = p->_conv_psi + conv_i * p->Nreal;
        
        for(int react_i=0; react_i<conv->n_reactants; react_i++)
        {   //take volume from reactants:
            udm_scalar *_phi = conv->reactants[react_i]->_phi;
            udm_scalar ratio = conv->r_ratios[react_i];
            switch(conv->type)
            {
                case global:
                case custom:
                    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(_phi, -ratio, _buffer, p->Nreal);
                    break;
                case local_left:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi, -ratio, _buffer, p->Nslice);
                    break;
                case local_right:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi+p->Nreal-p->Nslice, -ratio, _buffer, p->Nslice);
                    break;
                case transport_left:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi+p->Nreal/2-p->Nslice, -ratio, _buffer, p->Nslice);
                    break;
                case transport_right:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi+p->Nreal/2, -ratio, _buffer, p->Nslice);
                    break;
                default:;
            }
        }
        
        for(int prod_i=0; prod_i<conv->n_products; prod_i++)
        {   //give volume to products:
            udm_scalar *_phi = conv->products[prod_i]->_phi;
            udm_scalar ratio = conv->p_ratios[prod_i];
            switch(conv->type)
            {
                case global:
                case custom:
                    add_and_multiply_kernel1<<<p->blocks_single, p->threads>>>(_phi, ratio, _buffer, p->Nreal);
                    break;
                case local_left:
                case transport_left:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi, ratio, _buffer, p->Nslice);
                    break;
                case local_right:
                case transport_right:;
                    add_and_multiply_kernel1<<<p->blocks_slice, p->threads>>>(_phi+p->Nreal-p->Nslice, ratio, _buffer, p->Nslice);
                    break;
                default:;
            }
        }
    }
}

void evaluate_free_energy(PHASE *p)
{
    evaluate_mu(p); // To have the necessary quantities updated.
    evaluate_free_energy_density(p);
    update_host(p);

    udm_scalar Fe=0;
    
    for(int i=0; i<p->Nreal; i++)
    {
        Fe += p->fe[i];
    }
    p->Fe = Fe * p->dr[0] * p->dr[1] * p->dr[2];
}

void evaluate_free_energy_density(PHASE *p)
{
    initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->_fe, 0., p->Nreal);
    for(int ci=0; ci<p->n_components; ci++)
    {
        if(p->comps[ci]->arch == P1)
        {
            add_homopolymer_contribution_free_energy_kernel<<<p->blocks_single, p->threads>>>(p->_fe, p->_phi, p->_lap_psi, ci, p->N[0], p->N[1], p->N[2], p->dr[0], p->dr[1], p->dr[2], p->n_components, p->comps[ci]->params[0], p->comps[ci]->params[1], p->_chiN);
        } else if(p->comps[ci]->arch == P2)
        {
            add_diblock_copolymer_contribution_free_energy_kernel<<<p->blocks_single, p->threads>>>(p->_fe, p->_phi, p->_conv_psi, p->_lap_psi, ci, p->comps[ci]->bonded_comp_index, p->N[0], p->N[1], p->N[2], p->dr[0], p->dr[1], p->dr[2], p->n_components, p->comps[ci]->params[0], p->comps[ci]->params[1], p->comps[ci]->params[2], p->comps[ci]->params[3], p->comps[ci]->params[4], p->_chiN);
        } else 
        {
            printf("ERROR: %s: %d. Illegal component detected.", __FILE__, __LINE__);
        }
    }
}

void evaluate_mobility(PHASE *p)
{
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    for(int ic=0; ic<p->n_components; ic++)
    {
        COMPONENT *c=p->comps[ic];
        if( c->ddm_activated )
        {
            calculate_density_dependent_mobilty_kernel<<<p->blocks_single, p->threads>>>(c->_mobility, c->m/2., c->_ddm_parameters, p->_phi, p->n_components, p->Nreal);
#ifdef ENABLE_EVAPORATION_STABILIZATION
            initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->comps[0]->_mobility, 0, (p->interface_position-8)*p->Nslice);
            initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->comps[1]->_mobility, 0, (p->interface_position-8)*p->Nslice);
#ifndef ENABLE_REFLECTING_BC
            initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->comps[0]->_mobility+p->Nreal-(p->interface_position-8)*p->Nslice, 0, (p->interface_position-8)*p->Nslice);
            initialize_value_kernel<<<p->blocks_single, p->threads>>>(p->comps[1]->_mobility+p->Nreal-(p->interface_position-8)*p->Nslice, 0, (p->interface_position-8)*p->Nslice);
#endif //ENABLE_REFLECTING_BC
#endif //ENABLE_EVAPORATION_STABILIZATION
        } else 
        {
            initialize_value_kernel<<<p->blocks_single, p->threads>>>(c->_mobility, c->m, p->Nreal);
        }
    }
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
}
