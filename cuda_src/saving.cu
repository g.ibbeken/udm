/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <string.h>
#include <omp.h>
#include <complex.h>
#include <hdf5.h>

#include "saving.h"
#include "phase.h"
#include "helper.h"
#include "initialize_finalize.h"

#ifdef ENABLE_SINGLE_PRECISION
#define H5T_UDM_SCALAR H5T_NATIVE_FLOAT
#else //ENABLE_SINGLE_PRECISION
#define H5T_UDM_SCALAR H5T_NATIVE_DOUBLE
#endif //ENABLE_SINGLE_PRECISION

/*---Functions: ---------------------------------------------------------------------------------------------------------------------------------------------------*/

herr_t read_in_timeslab(PHASE *p)
{
    herr_t status;

    status    = H5Sselect_hyperslab(p->dspace, H5S_SELECT_SET, p->start, NULL, p->count, NULL);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
    status    = H5Dread(p->dset_phi, H5T_UDM_SCALAR, p->mspace, p->dspace, H5P_DEFAULT, p->phi);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    for(unsigned int i=0; i<p->Nfull; i++)
        if(isnan(p->phi[i]) || p->phi[i]<0){
            //printf("Nan or Inf detected on slab %d at pos %d.\n", slab, i);
            return -2;
        }
    reevaluate_averages(p);
    return(status);
}

void read_parameter_f(hid_t file, const char *name, udm_scalar *loc)
{
    herr_t status;
    hid_t dset = H5Dopen(file, name, H5P_DEFAULT); 
    hid_t dspace = H5Dget_space(dset);

    status    = H5Dread(dset, H5T_UDM_SCALAR, dspace, dspace, H5P_DEFAULT, loc);
    if(status<0){
        printf("ERROR: %s: %d for parameter %s\n", __FILE__, __LINE__, name);
        exit(1);
    }

    H5Sclose(dspace);
    H5Dclose(dset);
}

void read_parameter_i(hid_t file, const char *name, int *loc)
{
    herr_t status;
    hid_t dset = H5Dopen(file, name, H5P_DEFAULT); 
    hid_t dspace = H5Dget_space(dset);

    status    = H5Dread(dset, H5T_NATIVE_INT, dspace, dspace, H5P_DEFAULT, loc);
    if(status<0){
        printf("ERROR: %s: %d for parameter %s\n", __FILE__, __LINE__, name);
        exit(1);
    }

    H5Sclose(dspace);
    H5Dclose(dset);
}

void read_parameter_uint8(hid_t file, const char *name, uint8_t *loc)
{
    herr_t status;
    hid_t dset = H5Dopen(file, name, H5P_DEFAULT); 
    hid_t dspace = H5Dget_space(dset);

    status    = H5Dread(dset, H5T_NATIVE_UINT8, dspace, dspace, H5P_DEFAULT, loc);
    if(status<0){
        printf("ERROR: %s: %d for parameter %s\n", __FILE__, __LINE__, name);
        exit(1);
    }

    H5Sclose(dspace);
    H5Dclose(dset);
}

void read_last_parameter_i(hid_t file, const char *name, int *loc)
{
    herr_t status;
    hid_t dset = H5Dopen(file, name, H5P_DEFAULT); 
    hid_t dspace = H5Dget_space(dset);
    hsize_t dims;
    status = H5Sget_simple_extent_dims(dspace, &dims, NULL);
    hsize_t start = dims-1;
    hsize_t count = 1;
    hid_t mspace = H5Screate_simple(1, &count, NULL);
    H5Sselect_hyperslab(dspace, H5S_SELECT_SET, &start, NULL, &count, NULL);


    status    = H5Dread(dset, H5T_NATIVE_INT, mspace, dspace, H5P_DEFAULT, loc);
    if(status<0){
        printf("ERROR: %s: %d for parameter %s\n", __FILE__, __LINE__, name);
        exit(1);
    }

    H5Sclose(dspace);
    H5Dclose(dset);
}

int get_size_of_dset(hid_t file, const char *dset_name)
{
    herr_t status;
    hid_t dset = H5Dopen(file, dset_name, H5P_DEFAULT); 
    hid_t dspace = H5Dget_space(dset);
    hsize_t dims; //No arrays of rank higher than 6 possible (currently five highest) 

    status = H5Sget_simple_extent_dims(dspace, &dims, NULL);
    if(status<0){
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
        exit(1);
    }
    H5Sclose(dspace);
    H5Dclose(dset);
    return (int) dims;
}

void read_single_conversion(PHASE *p, int index)
{
    CONVERSION *conv = p->convs[index]; 

    conv->index = index;

    char conversion_group_name[20]; 
    sprintf(conversion_group_name, "conversions/conv%d/", index);
    
    //type:
    char type_dset_name[40]; 
    sprintf(type_dset_name, "%stype", conversion_group_name);
    int type;
    read_parameter_i(p->file, type_dset_name, &type);
    conv->type = (ConvType) type;


    //reactants:
    char reactants_dset_name[40]; 
    sprintf(reactants_dset_name, "%sreactants", conversion_group_name);
    conv->n_reactants = get_size_of_dset(p->file, reactants_dset_name);

    int *reactants_indices;
    if ((reactants_indices =   (int *)malloc(conv->n_reactants*sizeof(int*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_i(p->file, reactants_dset_name, reactants_indices);
    if ((conv->reactants =   (COMPONENT**)malloc(conv->n_reactants*sizeof(COMPONENT*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    for(int i=0; i<conv->n_reactants; i++)
    {
        conv->reactants[i] = p->comps[reactants_indices[i]];
    }
    free(reactants_indices);
    if ((conv->r_ratios =   (udm_scalar *)malloc(conv->n_reactants*sizeof(udm_scalar*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    char r_ratios_dset_name[40]; 
    sprintf(r_ratios_dset_name, "%sr_ratios", conversion_group_name);
    read_parameter_f(p->file, r_ratios_dset_name, conv->r_ratios);


    //products:
    char products_dset_name[40]; 
    sprintf(products_dset_name, "%sproducts", conversion_group_name);
    conv->n_products = get_size_of_dset(p->file, products_dset_name);

    int *products_indices;
    if ((products_indices =   (int *)malloc(conv->n_products*sizeof(int*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_i(p->file, products_dset_name, products_indices);
    if ((conv->products =   (COMPONENT**)malloc(conv->n_products*sizeof(COMPONENT*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    for(int i=0; i<conv->n_products; i++)
    {
        conv->products[i] = p->comps[products_indices[i]];
    }
    free(products_indices);
    if ((conv->p_ratios =   (udm_scalar *)malloc(conv->n_products*sizeof(udm_scalar*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    char p_ratios_dset_name[40]; 
    sprintf(p_ratios_dset_name, "%sp_ratios", conversion_group_name);
    read_parameter_f(p->file, p_ratios_dset_name, conv->p_ratios);

    //timescale:
    char reaction_rate_dset_name[40]; 
    sprintf(reaction_rate_dset_name, "%sreaction_rate", conversion_group_name);
    if(H5Lexists(p->file, reaction_rate_dset_name, H5P_DEFAULT) > 0)
    {
        read_parameter_f(p->file, reaction_rate_dset_name, &(conv->reaction_rate));
    } else
    {
        //include the case that timescale is given instead of reaction_rate for backward compatibility.
        sprintf(reaction_rate_dset_name, "%stimescale", conversion_group_name);
        read_parameter_f(p->file, reaction_rate_dset_name, &(conv->reaction_rate));
        conv->reaction_rate = 1/conv->reaction_rate;
    }

    //factor:
    conv->factor = p->dt * conv->reaction_rate;
    

    //custom domain read if ConvType is set to custom.
    if( conv->type == custom )
    {
        char custom_domain_dset_name[40]; 
        sprintf(custom_domain_dset_name, "%scustom_domain", conversion_group_name);
        if ((conv->custom_domain =   (uint8_t*)malloc(p->Nreal*sizeof(uint8_t*)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                    exit(1);
            }
        read_parameter_uint8(p->file, custom_domain_dset_name, conv->custom_domain);
    } else 
    {
        conv->custom_domain = NULL;
    }
}

void read_conversions(PHASE *p)
{
#ifdef ENABLE_CONVERSIONS
    //read number of conversions:
    read_parameter_i(p->file, "conversions/n_conversions/", &(p->n_conversions));
    //And allocate memory for the pointers:
    if ((p->convs  =   (CONVERSION**)malloc(p->n_conversions*sizeof(CONVERSION*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    for(int i=0; i<p->n_conversions; i++) {
        if ((p->convs[i]  =   (CONVERSION*)malloc(sizeof(CONVERSION)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                    exit(1);
            }
        //printf("Reading conversion %d.\n", i);
        //fflush(stdout);
        read_single_conversion(p, i);
    }
#endif //ENABLE_CONVERSIONS
}

void read_hdf5(PHASE *p)
{
    //Read in input file
    if(access(p->fname, F_OK) == -1){
        printf("ERROR: Cannot access input file.");
        //exit(1);
    }
    p->file = H5Fopen(p->fname, H5F_ACC_RDWR, H5P_DEFAULT);

    if(H5Lexists(p->file, "version", H5P_DEFAULT) > 0)
        read_parameter_i(p->file, "version", &(p->version));
    else
        p->version = 0;
    
    if ( p->version != 0 & p->version != 1)
    {
        printf("ERROR: version %d unknown. Please specify available version in .xml file.\n", p->version);
        return;
    }

    p->n_components = get_size_of_dset(p->file, "parameter/type");

    //Read parameters from file
    read_parameter_f(p->file, "/parameter/Tsave", &(p->Tsave));
    read_parameter_f(p->file, "/parameter/Lxyz", p->L);
    read_parameter_i(p->file, "/parameter/Nxyz", p->N);
    read_parameter_f(p->file, "/parameter/T", &(p->T));
    read_parameter_f(p->file, "/parameter/dt", &(p->dt));
    //==== Step size of the individual steps that make one time step:


    p->Nt    = llround(p->T/p->dt);
    p->dr[0] = p->L[0]/p->N[0];
    p->dr[1] = p->L[1]/p->N[1];
    p->dr[2] = p->L[2]/p->N[2];

    p->Nreal    = p->N[0]*p->N[1]*p->N[2];
#ifndef ENABLE_REFLECTING_BC
    if( p->N[2] == 1 )
    {
        if( p->N[1] == 1 )
        {
            p->Ncomplex = p->N[0]/2+1;
            p->dim = 1;
        } else
        {
            p->Ncomplex = p->N[0]*(p->N[1]/2+1);
            p->dim = 2;
        }
    } else
    {
        p->Ncomplex = p->N[0]*p->N[1]*(p->N[2]/2+1);
        p->dim = 3;
    }
#else //IF ENABLE_REFLECTING_BC
    if( p->N[2] == 1 )
    {
        if( p->N[1] == 1 )
        {
            p->Ncomplex = p->N[0]+1;
            //p->Ncomplex = p->N[0];
            p->dim = 1;
        } else
        {
            p->Ncomplex = 2*p->N[0]*(p->N[1]/2+1);
            //p->Ncomplex = (2*p->N[0]-2)*(p->N[1]/2+1);
            p->dim = 2;
        }
    } else
    {
        p->Ncomplex = 2*p->N[0]*p->N[1]*(p->N[2]/2+1);
        //p->Ncomplex = (2*p->N[0]-2)*p->N[1]*(p->N[2]/2+1);
        p->dim = 3;
    }
#endif //ENABLE_REFLECTING_BC

    p->Nslice   = p->N[1]*p->N[2];
    p->Nfull    = p->n_components*p->Nreal;

    p->oodz32  = 1./p->dr[0]/sqrt(p->dr[0] * p->dr[1] * p->dr[2]); // these include the terms oosqrtdxdydz 
    p->oody32  = 1./p->dr[1]/sqrt(p->dr[0] * p->dr[1] * p->dr[2]); // from the discretization of delta function in variance
    p->oodx32  = 1./p->dr[2]/sqrt(p->dr[0] * p->dr[1] * p->dr[2]);

    p->oodz2  = 1./(p->dr[0] * p->dr[0]);
    p->oody2  = 1./(p->dr[1] * p->dr[1]);
    p->oodx2  = 1./(p->dr[2] * p->dr[2]);

    if ( p->version == 0 )
    {
        p->oodz32  = 1./p->dr[0]; // this reproduces the bug of version 0!!!
        p->oody32  = 1./p->dr[1];
        p->oodx32  = 1./p->dr[2];
    }

    p->cutoff = 2;

    //First create space to read out component specific parameters, then create the structs with this later.
    int *type;
    if ((type  =   (int*)malloc(p->n_components*sizeof(int)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_i(p->file, "/parameter/type", type);
    int *corr_comp;
    if ((corr_comp  =   (int*)malloc(p->n_components*sizeof(int)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_i(p->file, "/parameter/corr_comp", corr_comp);
    int *N;
    if ((N  =   (int*)malloc(p->n_components*sizeof(int)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_i(p->file, "/parameter/N", N);
    udm_scalar *f;
    if ((f  =   (udm_scalar*)malloc(p->n_components*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_f(p->file, "/parameter/f", f);
    udm_scalar *rho;
    if ((rho  =   (udm_scalar*)malloc(p->n_components*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_f(p->file, "/parameter/rho", rho);
    udm_scalar *mobility;
    if ((mobility  =   (udm_scalar*)malloc(p->n_components*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_f(p->file, "/parameter/mobility", mobility);

    int *ddm_activated;
    if ((ddm_activated =   (int*)malloc(p->n_components*sizeof(int)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    if(H5Lexists(p->file, "parameter/density_dependent_mobility_activated", H5P_DEFAULT) > 0)
    {
        read_parameter_i(p->file, "/parameter/density_dependent_mobility_activated", ddm_activated);
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
        for(int ic=0; ic<p->n_components; ic++)
        {
            if( ddm_activated[ic] != 0 )
            {
                fprintf(stderr,"ERROR: Density-dependent mobility activated in simulation setup but not for executable.\n", __LINE__);
                exit(1);
            }
        }
#endif //NOT ENABLE_DENSITY_DEPENDENT_MOBILITY
    }

#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    udm_scalar *ddm_params;
    if ((ddm_params =   (udm_scalar*)malloc(p->n_components*(p->n_components+1)*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    read_parameter_f(p->file, "/parameter/density_dependent_mobility_parameters", ddm_params);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY


#ifdef ENABLE_NOISE
    if ( p->version == 0 )
    {
        if(H5Lexists(p->file, "parameter/effective_temperature", H5P_DEFAULT) > 0)
        {
             read_parameter_f(p->file, "/parameter/effective_temperature", &(p->effective_temp));
             p->noise_prefactor = sqrt(p->dt * p->effective_temp); 
             p->sqrtnbar = p->oodx32*p->oody32*p->oodz32 / (2. * N[0] * p->effective_temp);
             printf("WARNING: Version 0 of the UDM is depricated (the noise amplitude is dependent on spatial discretization). Consider using version 1 or newer.\n");
        }
        else
        {
            printf("WARNING: noise activated but no effective temperature given. Specify <sqrtnbar> in xml-file or disable thermal noise and recompile to continue.\n");
            exit(1);
        }
    } else {
        if(H5Lexists(p->file, "parameter/sqrtnbar", H5P_DEFAULT) > 0)
        {
             read_parameter_f(p->file, "/parameter/sqrtnbar", &(p->sqrtnbar));
             p->noise_prefactor = sqrt(p->dt / (2 * p->sqrtnbar));
        }
        else
        {
            printf("WARNING: noise activated but no effective temperature given. Specify <sqrtnbar> in xml-file or disable thermal noise and recompile to continue.\n");
            exit(1);
        }
    }

#endif //ENABLE_NOISE

    //Create memory for phi and mu and read out file
    if ((p->phi  =   (udm_scalar*)malloc(p->Nfull*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    if ((p->mu  =   (udm_scalar*)malloc(p->Nfull*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    if ((p->fe  =   (udm_scalar*)malloc(p->Nreal*sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    p->dset_phi = H5Dopen(p->file, "phi", H5P_DEFAULT); 
    p->dspace = H5Dget_space(p->dset_phi);

    //Check, wether dimensions match the one from the previous simulations. 
    //Then create mspace and start, count. 

    const unsigned int ndimsf = H5Sget_simple_extent_ndims(p->dspace);
    if (ndimsf != 5){
        printf("ERROR: %s:%d not the correct number of dimensions in file opening.\n", __FILE__, __LINE__);
        exit(1);
    }
    hsize_t dimsf[5];
    H5Sget_simple_extent_dims(p->dspace, dimsf, NULL);
    if (dimsf[1] != p->n_components || dimsf[2]!=p->N[0] || dimsf[3]!=p->N[1] || dimsf[4]!=p->N[2]){
        printf("ERROR: %s:%d Something does not match (c_components or Nxyz).\n", __FILE__, __LINE__);
        printf("ERROR: %s:%d Got: %lld %lld %lld %lld .\n", __FILE__, __LINE__, dimsf[1], dimsf[2], dimsf[3], dimsf[4]);
        exit(1);
    }
    hsize_t dimm[4] = {dimsf[1],dimsf[2],dimsf[3],dimsf[4]};
    p->mspace = H5Screate_simple(4, dimm, NULL); 

    p->Tstart = (dimsf[0]-1)*p->Tsave;
    p->tsave= (int)(p->Tsave/p->dt);//Time steps between the saved time slabs.

    p->start[0] = dimsf[0]-1;
    p->start[1] = 0;
    p->start[2] = 0;
    p->start[3] = 0;
    p->start[4] = 0;
    p->count[0] = 1;
    p->count[1] = p->n_components;
    p->count[2] = p->N[0];
    p->count[3] = p->N[1];
    p->count[4] = p->N[2];

    if(H5Lexists(p->file, "external_field", H5P_DEFAULT) > 0)
    {
#ifndef ENABLE_EXTERNAL_FIELD
        printf("ERROR: External field is defined but compile option (ENABLE_EXTERNAL_FIELD) was not chosen. Please recompile correctly or remove external field from h5-file to continue.\n");
        exit(1);
#else //ENABLE_EXTERNAL_FIELD
        if ((p->external_field=   (udm_scalar*)malloc(p->Nfull*sizeof(udm_scalar)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory\n");
                    exit(1);
            }
    } else
    {
        p->external_field = NULL;
#endif //ENABLE_EXTERNAL_FIELD
    }
    if(H5Lexists(p->file, "wall", H5P_DEFAULT) > 0)
    {
#ifndef ENABLE_EXTERNAL_FIELD
        printf("ERROR: Wall is defined but compile option (ENABLE_EXTERNAL_FIELD) was not chosen. Please recompile correctly or remove external field from h5-file to continue.\n");
        exit(1);
#else //ENABLE_EXTERNAL_FIELD
        printf("Wall activated.\n");
        if ((p->wall=   (uint8_t*)malloc(p->Nreal*sizeof(uint8_t)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory\n");
                    exit(1);
            }
    } else
    {
        p->wall = NULL;
#endif //ENABLE_EXTERNAL_FIELD
    }

    if(H5Lexists(p->file, "umbrella", H5P_DEFAULT) > 0)
    {
#ifndef ENABLE_UMBRELLA_FIELD
        printf("ERROR: Umbrella field is defined but compile option (ENABLE_UMBRELLA_FIELD) was not chosen. Please recompile correctly or remove external field from h5-file to continue.\n");
        exit(1);
#else //ENABLE_EXTERNAL_FIELD
        if ((p->umbrella_field=   (udm_scalar*)malloc(p->Nfull*sizeof(udm_scalar)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory\n");
                    exit(1);
            }
    } else
    {
        p->umbrella_field = NULL;
#endif //ENABLE_EXTERNAL_FIELD
    }


    //For datapace creation set chunking properties (necessary to expand the datasets on-the-fly):
    p->chunk_prop = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(p->chunk_prop, 5, dimsf);

    //Now set up components:
    if ((p->comps  =   (COMPONENT**)malloc(p->n_components*sizeof(COMPONENT*)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    for(int i=0; i<p->n_components; i++) {
        if ((p->comps[i]  =   (COMPONENT*)malloc(sizeof(COMPONENT)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                    exit(1);
            }
        PolyArch arch;
        switch(type[i]){
            case 1:
                arch = P1;
                break;
            case 2:
                arch = P2;
                break;
            default:;
        }
        set_up_component(p, i, arch, N[i], f[i], rho[i], mobility[i], corr_comp[i]);
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
        p->comps[i]->ddm_activated = ddm_activated[i];
        if ((p->comps[i]->ddm_parameters  =   (udm_scalar*)malloc((p->n_components+1) * sizeof(udm_scalar)))==NULL) {
                    fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                    exit(1);
            }
        for(int ip=0; ip<p->n_components+1; ip++)
        {
            p->comps[i]->ddm_parameters[ip] = ddm_params[i*(p->n_components+1)+ip];
        }
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    }

    //Read order parameter field data from file:
    read_in_timeslab(p);

    //Allocate and read_in chiN
    if ((p->chiN  =   (udm_scalar*)malloc(p->n_components * p->n_components * sizeof(udm_scalar)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    if(H5Lexists(p->file, "/parameter/chi", H5P_DEFAULT) > 0)
    {
        read_parameter_f(p->file, "/parameter/chi", p->chiN);
        for(unsigned int i=0; i<p->n_components*p->n_components; i++)
        {
            p->chiN[i] *= p->comps[0]->N;//new version uses chiN
        }
    } else 
    {
        read_parameter_f(p->file, "/parameter/chiN", p->chiN);
    }

    free(type);
    free(corr_comp);
    free(N);
    free(f);
    free(rho);
    free(mobility);
    free(ddm_activated);
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    free(ddm_params);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY

#ifdef ENABLE_CONVERSIONS
    read_conversions(p);
#endif //ENABLE_CONVERSIONS
#ifdef ENABLE_EXTERNAL_FIELD
    if(p->external_field != NULL)
    {
        read_parameter_f(p->file, "external_field", p->external_field);
    }
    if(p->wall != NULL)
    {
        read_parameter_uint8(p->file, "wall", p->wall);
    }
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if(p->umbrella_field != NULL){
        read_parameter_f(p->file, "umbrella/field", p->umbrella_field);
        read_parameter_f(p->file, "umbrella/kappa", &(p->umbrella_kappa));
    }
#endif //ENABLE_EXTERNAL_FIELD

#ifdef ENABLE_EVAPORATION_STABILIZATION
    if( H5Lexists(p->file, "parameter/conversion_delay", H5P_DEFAULT) > 0 )
    {
        read_parameter_i(p->file, "parameter/conversion_delay", &(p->conversion_delay));
        read_last_parameter_i(p->file, "shift", &(p->shift));
    } else 
    {
        p->conversion_delay = -1;
        p->shift = 0;
    }
    if( p->shift > 0 )
    {
        p->interface_position = p->conversion_delay;
    } else
    {
        p->interface_position = 0;
    }
#endif //ENABLE_EVAPORATION_STABILIZATION
       //
}

herr_t expand_dataset(PHASE *p, const char *dset_name, udm_scalar *data, int ncomps)
{
    herr_t      status;
    hid_t mspace, dspace, dset; 

    dset = H5Dopen(p->file, dset_name, H5P_DEFAULT); 
    dspace = H5Dget_space(dset);

    const unsigned int ndimsf = H5Sget_simple_extent_ndims(dspace);
    //if (ndimsf != 5){
    //j    printf("ERROR: %s:%d not the correct number of dimensions to extent the data set.\n", __FILE__, __LINE__);
    //    return -1;
    //}
    hsize_t *dimsf;
    if ((dimsf  =   (hsize_t*)malloc(ndimsf*sizeof(hsize_t)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    status = H5Sget_simple_extent_dims(dspace, dimsf, NULL);
    dimsf[0] += 1;

    status = H5Dset_extent(dset, dimsf);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    status = H5Sclose(dspace);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
        
    dspace = H5Dget_space(dset);
    mspace = H5Screate_simple(ndimsf-1, dimsf+1, NULL); 
    
    hsize_t start[] = {dimsf[0]-1, 0, 0, 0, 0};
    hsize_t count[] = {1, (hsize_t) ncomps, (hsize_t) p->N[0], (hsize_t) p->N[1], (hsize_t) p->N[2]};
    status    = H5Sselect_hyperslab(dspace, H5S_SELECT_SET, start, NULL, count, NULL);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
    status    = H5Dwrite(dset, H5T_UDM_SCALAR, mspace, dspace, H5P_DEFAULT, data);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    H5Dclose(dset);
    H5Sclose(dspace);
    H5Sclose(mspace);
    free(dimsf);

    return 0;

}

herr_t save_dataset(PHASE *p, const char *dset_name, udm_scalar *data, int ncomps)
{
    herr_t      status;
    hid_t mspace, dspace, dset; 

    dset = H5Dopen(p->file, dset_name, H5P_DEFAULT); 
    dspace = H5Dget_space(dset);

    const unsigned int ndimsf = H5Sget_simple_extent_ndims(dspace);
    //if (ndimsf != 5){
    //j    printf("ERROR: %s:%d not the correct number of dimensions to extent the data set.\n", __FILE__, __LINE__);
    //    return -1;
    //}
    hsize_t *dimsf;
    if ((dimsf  =   (hsize_t*)malloc(ndimsf*sizeof(hsize_t)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    status = H5Sget_simple_extent_dims(dspace, dimsf, NULL);
    mspace = H5Screate_simple(ndimsf-1, dimsf+1, NULL); 
    
    hsize_t start[] = {dimsf[0]-1, 0, 0, 0, 0};
    hsize_t count[] = {1, (hsize_t) ncomps, (hsize_t) p->N[0], (hsize_t) p->N[1], (hsize_t) p->N[2]};
    status    = H5Sselect_hyperslab(dspace, H5S_SELECT_SET, start, NULL, count, NULL);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
    status    = H5Dwrite(dset, H5T_UDM_SCALAR, mspace, dspace, H5P_DEFAULT, data);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    H5Dclose(dset);
    H5Sclose(dspace);
    H5Sclose(mspace);
    free(dimsf);

    return 0;
}

herr_t write_custom_domain(PHASE *p, CONVERSION *cv)
{
    herr_t      status;
    hid_t mspace, dspace, dset; 
    char dset_name[40];
    sprintf(dset_name, "conversions/conv%d/custom_domain", cv->index);

    dset = H5Dopen(p->file, dset_name, H5P_DEFAULT); 
    dspace = H5Dget_space(dset);

    const unsigned int ndimsf = H5Sget_simple_extent_ndims(dspace);
    hsize_t *dimsf;
    if ((dimsf  =   (hsize_t*)malloc(ndimsf*sizeof(hsize_t)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    status = H5Sget_simple_extent_dims(dspace, dimsf, NULL);
    mspace = H5Screate_simple(ndimsf, dimsf, NULL); 
    
    status    = H5Dwrite(dset, H5T_NATIVE_UINT8, mspace, dspace, H5P_DEFAULT, cv->custom_domain);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    H5Dclose(dset);
    H5Sclose(dspace);
    H5Sclose(mspace);
    free(dimsf);
    H5Fflush(p->file, H5F_SCOPE_GLOBAL);

    return 0;
}

herr_t expand_dataset_i(PHASE *p, const char *dset_name, int *data, int ncomps)
{
    herr_t      status;
    hid_t mspace, dspace, dset; 

    dset = H5Dopen(p->file, dset_name, H5P_DEFAULT); 
    dspace = H5Dget_space(dset);

    const unsigned int ndimsf = H5Sget_simple_extent_ndims(dspace);
    //if (ndimsf != 5){
    //j    printf("ERROR: %s:%d not the correct number of dimensions to extent the data set.\n", __FILE__, __LINE__);
    //    return -1;
    //}
    hsize_t *dimsf;
    if ((dimsf  =   (hsize_t*)malloc(ndimsf*sizeof(hsize_t)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory in line %d.\n", __LINE__-1);
                exit(1);
        }
    status = H5Sget_simple_extent_dims(dspace, dimsf, NULL);
    dimsf[0] += 1;

    status = H5Dset_extent(dset, dimsf);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    status = H5Sclose(dspace);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
        
    dspace = H5Dget_space(dset);
    mspace = H5Screate_simple(ndimsf-1, dimsf+1, NULL); 
    
    hsize_t start[] = {dimsf[0]-1, 0, 0, 0, 0};
    hsize_t count[] = {1, (hsize_t) ncomps, (hsize_t) p->N[0], (hsize_t) p->N[1], (hsize_t) p->N[2]};
    status    = H5Sselect_hyperslab(dspace, H5S_SELECT_SET, start, NULL, count, NULL);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);
    status    = H5Dwrite(dset, H5T_NATIVE_INT, mspace, dspace, H5P_DEFAULT, data);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);

    H5Dclose(dset);
    H5Sclose(dspace);
    H5Sclose(mspace);
    free(dimsf);

    return 0;

}

herr_t write_timeslab(PHASE *p)
{
    for(unsigned int i=0; i<p->Nfull; i++)
        if(isnan(p->phi[i])){
            printf("Nan detected at pos %d.\n", i);
            return -2;
        }
    herr_t status = expand_dataset(p, "phi", p->phi, p->n_components);
    if(status<0)
        printf("ERROR: %s: %d\n", __FILE__, __LINE__);


    if(H5Lexists(p->file, "free_energy", H5P_DEFAULT) > 0)
    {
        //printf("Found free energy dataset. Expanding and saving fe=%f.\n", p->Fe);
        status = expand_dataset(p, "free_energy", &(p->Fe), 1);
    }
#ifdef ENABLE_EVAPORATION_STABILIZATION
    if(H5Lexists(p->file, "shift", H5P_DEFAULT) > 0)
    {
        status = expand_dataset_i(p, "shift", &(p->shift), 1);
    }
#endif //ENABLE_EVAPORATION_STABILIZATION

    if(H5Lexists(p->file, "chemical_potential", H5P_DEFAULT) > 0)
    {
        
#ifdef ENABLE_UMBRELLA_FIELD
        if( p->umbrella_field == NULL )
        {
            for(int i=0; i<p->Nfull; i++)
            {
                if( p->phi[i] > 0 )
                {
                    p->mu[i] /= 2 * sqrt(p->phi[i]);
                }
            }
        } else 
        {
            for(int i=0; i<p->Nfull; i++)
            {
                p->mu[i] = - p->umbrella_kappa * (p->phi[i] - p->umbrella_field[i]);
            }
            
        }
#else   //ENABLE_UMBRELLA_FIELD
        for(int i=0; i<p->Nfull; i++)
        {
            if( p->phi[i] > 0 )
            {
                p->mu[i] /= 2 * sqrt(p->phi[i]);
            }
        }       
#endif   //ENABLE_UMBRELLA_FIELD
        status = expand_dataset(p, "chemical_potential", p->mu, p->n_components);
    }

    if(H5Lexists(p->file, "free_energy_density", H5P_DEFAULT) > 0)
    {
        status = expand_dataset(p, "free_energy_density", p->fe, 1);
    }
    H5Fflush(p->file, H5F_SCOPE_GLOBAL);
    
    return status;
    }

herr_t save_first_potentials(PHASE *p)
{
    herr_t      status;
    if(H5Lexists(p->file, "free_energy", H5P_DEFAULT) > 0)
    {
        status = save_dataset(p, "free_energy", &(p->Fe), 1);
    }
    if(H5Lexists(p->file, "chemical_potential", H5P_DEFAULT) > 0)
    {
        
#ifdef ENABLE_UMBRELLA_FIELD
        if( p->umbrella_field == NULL )
        {
            for(int i=0; i<p->Nfull; i++)
            {
                if( p->phi[i] > 0 )
                {
                    p->mu[i] /= 2 * sqrt(p->phi[i]);
                }
            }
        } else 
        {
            for(int i=0; i<p->Nfull; i++)
            {
                p->mu[i] = - p->umbrella_kappa * (p->phi[i] - p->umbrella_field[i]);
            }
            
        }
#else   //ENABLE_UMBRELLA_FIELD
        for(int i=0; i<p->Nfull; i++)
        {
            if( p->phi[i] > 0 )
            {
                p->mu[i] /= 2 * sqrt(p->phi[i]);
            }
        }       
#endif   //ENABLE_UMBRELLA_FIELD
        status = save_dataset(p, "chemical_potential", p->mu, p->n_components);
    }

    if(H5Lexists(p->file, "free_energy_density", H5P_DEFAULT) > 0)
    {
        status = save_dataset(p, "free_energy_density", p->fe, 1);
    }
    H5Fflush(p->file, H5F_SCOPE_GLOBAL);

    return status;
}

void close_file(PHASE *p)
{
    H5Pclose(p->chunk_prop);
    H5Sclose(p->dspace);
    H5Sclose(p->mspace);
    H5Dclose(p->dset_phi);
    H5Fclose(p->file); 
}
