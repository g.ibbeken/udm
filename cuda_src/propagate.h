/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROPAGATE_H
#define PROPAGATE_H

#include "phase.h"
/*---Function Declarations: -------------------------------------------------------------------------------------------------------------------------------------*/

/** Executes the full time loop, after initial saving of observables, if necessary.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 * \return error code, 0 if successfull, non-zero if not.
 */
int simulate(PHASE *p);

/** Executes a single time step, i.e. 1 Euler time step.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void time_step(PHASE *p);

/** Evaluates the chemical potential of each component according to eqs. (5), (6), and (7) 
 *  in model_introduction.pdf. Saves the result on device in `p->_mu`.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_mu(PHASE *p);

/** Evaluates helper variables for the calculation of the chemical potential of each compo-
 *  nent, such as pi(r) (i.e. eqs. (20) and (21) in model_introduction.pdf), `psi` and their
 *  laplacians and the convolution of p->_psi with the long-range kernel if a component is 
 *  part of a block copolymer. Saves the result on device in the respective variables.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void precalculation(PHASE *p);

/** Evaluates the (numerically approximated) Lagrange multiplier, pi(r) by eqs. (20) and 
 * (21) in model_introduction.pdf. Saves on dives in `p->_dphi`. 
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void calculate_density_variation(PHASE *p);

/** Evaluates the chemical potential of a given component that is part of a diblock copoly-
 * mer according (6) and (7) in model_introduction.pdf. Saves the result on device in 
 * `c->_mu`. Each kernel call in this function corresponds to a term/summand in the chemi-
 * cal potential.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 * \param c Fully intitialized component structure (COMPONENT).
 */
void evaluate_mu_diblock(PHASE *p, COMPONENT *c);

/** Evaluates the chemical potential of a given component that is a homopolymer according 
 * eq. (5) in model_introduction.pdf. Saves the result on device in `c->_mu`. Each kernel
 * call in this function corresponds to a term/summand in the chemical potential.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 * \param c Fully intitialized component structure (COMPONENT).
 */
void evaluate_mu_homopolymer(PHASE *p, COMPONENT *c);

/** Add the external-field contribution to the readily-evaluated chemical potentials, 
 * eq. (24) in model_introduction.pdf. The external field that is read from the input file
 * at start was saved in p->_external_field and is simply used in this function (calling 
 * a kernel).
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void add_external_field_contribution(PHASE *p);

/** Add the umbrella-potential contribution to the readily-evaluated chemical potentials,
 *          mu_c(r) += kappa0* N0 * (phi_c^0(r) - phi_c(r))
 * where kappa0 is a scalar and phi_c^0 is the target concentration field, both are read 
 * from input file and saved in p->_umbrella_field and is simply used in this function 
 * (calling a kernel). This is not described in model_introduction.pdf.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void add_umbrella_field_contribution(PHASE *p);

/** Evaluates the Laplacian of each chemical potential on device. This function is only 
 * called if compile option ENABLE_DENSITY_DEPENDENT_MOBILITY is not chosen, i.e. this 
 * function calculates the Laplacian in Fourier space.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_laplace_mu(PHASE *p);

/** Evaluates the explicit increment of each component on device. The explicit increment
 * is the r.h.s of eq. (11) in model_introduction. This saves the result in c->_conv_psi
 * where c is the pointer to the COMPONENT struct for each component. This function re-
 * quires evaluate_mu() to be called beforeahand. 
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_explicit_increment(PHASE *p);

/** Evaluates the propagated concentration field (unregularized and excluding conversions
 * /reactions) by first evaluating the implicit increment, the second term of eq. (16) in 
 * model_introduction.pdf and subsequently the increment, eq. (16) for  each component on 
 * device. This updates the result in p->_phi. This function requires evaluate_explicit_
 * increment() to be called beforeahand. 
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_implicit_method(PHASE *p);

/** Calls the regularization kernel which performs it as described in model_introduction.pdf.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void regularize_phi(PHASE *p);

/** Perform conversions as specified in input file and saved as CONVERSION structs in 
 * p->convs. These fulfill mass-action kinetics.
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void perform_conversions(PHASE *p);

/** Evaluate the total free energy (spatially integrated free-energy density). Saved in 
 * p->Fe on host. This function itself calls evaluate_mu() and evaluate_free_energy_
 * density(), and updates host (copy chem pot and concentration fields to host).
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_free_energy(PHASE *p);

/** Evaluate the free-energy density on device. Saved in p->_fe. 
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_free_energy_density(PHASE *p);

/** Evaluate the (spatially dependent) mobility. This only executes if ENABLE_DENSITY_
 * DEPENDENT_MOBILITY option is specified. If mobility modifier is specified in input 
 * file, its evaluating this. Otherwise the constant value lambda_c (c->m) is written. 
 *
 * \param p Fully intitialized simulation structure (PHASE).
 */
void evaluate_mobility(PHASE *p);

#endif // PROPAGATE_H
