/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "phase.h"
#include <stdio.h>
#include <stdlib.h>

void print_component_setup(PHASE *p, COMPONENT *c)
{
    printf("Component %d:", c->index);
    if(c->arch==P2)
        printf("Diblock copolymer with component %d. N=%d,\t, f=%.2f,\trho=%.5f,\tm=%.1f", c->bonded_comp_index, c->N, c->f, c->rho, c->m);
    else if(c->arch==P1)
        printf("Homopolymer. N=%d,\t, rho=%.5f,\tm=%.1f", c->N, c->rho, c->m);
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    if( c->ddm_activated )
    {
        printf(", ddm activated with params %.1e", c->ddm_parameters[0]);
        for(int i=0; i<p->n_components; i++)
        {
            printf(" %.1e", c->ddm_parameters[i+1]);
        }
    }
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    printf(".\n");
}

void print_conversion_setup(CONVERSION *c)
{
    printf("Conversion: ");
    for(int react_i=0; react_i<c->n_reactants; react_i++)
    {
        printf("%f * %d", c->r_ratios[react_i], c->reactants[react_i]->index);
        if(react_i<c->n_reactants-1)
            printf(" + ");
    }
    printf(" -->  ");
    for(int prod_i=0; prod_i<c->n_products; prod_i++)
    {
        printf("%f * %d", c->p_ratios[prod_i], c->products[prod_i]->index);
        if(prod_i<c->n_products-1)
            printf(" + ");
    }
    printf("\t(Reaction rate r=%f -->factor=%e)", c->reaction_rate, c->factor);
    printf("\n");
}

void print_setup(PHASE *p)
{
    printf("UDM Configuration: ");
#ifdef ENABLE_SINGLE_PRECISION  
    printf("SINGLE PRECISION | ");
#else //ENABLE_SINGLE_PRECISION
    printf("DOUBLE PRECISION | ");
#endif //ENABLE_SINGLE_PRECISION
#ifdef ENABLE_NOISE
    printf("NOISE: sqrt(nbar)=%e | ", p->sqrtnbar);
#else //ENABLE_NOISE
    printf("NO NOISE | ");
#endif //ENABLE_NOISE
#ifdef ENABLE_REFLECTING_BC
    printf("REFLECTING BC | ");
#else  //ENABLE_REFLECTING_BC
    printf("PERIODIC BC | ");
#endif //ENABLE_REFLECTING_BC
#ifdef ENABLE_CONVERSIONS
    printf("CONVERSIONS ALLOWED | ");
#endif //ENABLE_UMBRELLA_FIELD
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    printf("DENSITY-DEPENDENT MOBILITY | ");
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
#ifdef ENABLE_EVAPORATION_STABILIZATION
    printf("STABILIZE GAS PHASE | ");
#endif //ENABLE_EVAPORATION_STABILIZATION

#ifdef ENABLE_EXTERNAL_FIELD
    if (p->external_field != NULL)
        printf("EXTERNAL FIELD ACTIVATED | ");
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if (p->umbrella_field != NULL)
        printf("UMBRELLA FIELD ACTIVATED | ");
#endif //ENABLE_UMBRELLA_FIELD
    printf("\nSimulating file %s.\n", p->fname);
    printf("============================================\n System Parameters: \n\n");
    printf("Lz=%f, Ly=%f, Lx=%f, Nz=%d, Ny=%d, Nx=%d\n", p->L[0], p->L[1], p->L[2], p->N[0], p->N[1], p->N[2]);
    printf("T=%f, dt=%f, Tsave=%f, T0 = %f\n", p->T, p->dt, p->Tsave, p->Tstart);
    printf("============================================\n %d Components: \n\n", p->n_components);
    for(int comp_i=0; comp_i<p->n_components; comp_i++)
    {
        print_component_setup(p, p->comps[comp_i]);
    }
    printf("============================================\n xN: \n\n");
    for(int comp_i=0; comp_i<p->n_components; comp_i++)
    {
        for(int comp_j=0; comp_j<p->n_components; comp_j++)
        {
            printf("%f\t", p->chiN[comp_i*p->n_components+comp_j]);
        }
        printf("\n");
    }
#ifdef ENABLE_CONVERSIONS 
    printf("============================================\n %d Conversions: \n\n", p->n_conversions);
    for(int conv_i=0; conv_i<p->n_conversions; conv_i++)
    {
        print_conversion_setup(p->convs[conv_i]);
    }
#endif

    if( p->version < 1 )
    {
        printf("\n");
        printf("|====================================================================================================================|\n");
        printf("|                                                                                                                    |\n");
        printf("|  WARNING: UDM Version 0.0.0 is depricated (missleading time scale) --> consider using version 1.0.0 in .xml file.  |\n");
        printf("|  INFO: when updating, scale all times by factor 1/N[0] (polymerization of first component).                        |\n");
        printf("|                                                                                                                    |\n");
        printf("|====================================================================================================================|\n");
        printf("\n");
    }
    
    fflush(stdout);
}

void print_copyright_notice()
{
    printf("Uneyama-Doi Model (UDM): continuum model simulations of polymer melts and solutions.\n");
    printf("Copyright (c) 2020-2024 Gregor Haefner\n\n");
}

void set_up_component(PHASE *p, int comp_index, PolyArch arch, int N, udm_scalar f, udm_scalar rho, udm_scalar m, int corr_comp_index)
{
    COMPONENT *c;
    c = p->comps[comp_index];
    c->index = comp_index;

    c->arch = arch;
    c->N = N;
    c->f =f ;
    c->rho = rho;
#ifdef ENABLE_EVAPORATION_STABILIZATION
    c->rhobulk = rho;
#endif //ENABLE_EVAPORATION_STABILIZATION
    c->m = m;

    c->bonded_comp_index = corr_comp_index;

    c->phi = p->phi + comp_index * p->Nreal;
}
