/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <hdf5.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <math.h>
#include <complex.h>


#include "initialize_finalize.h"
#include "phase.h"
#include "helper.h"
#include "saving.h"
#include "propagate.h"
#include "random.h"

#define PI 3.14159265
#define N_THREADS 256

#ifdef ENABLE_SINGLE_PRECISION
#define CUFFT_HANDLE_R2C CUFFT_R2C
#define CUFFT_HANDLE_C2R CUFFT_C2R
#else //ENABLE_SINGLE_PRECISION
#define CUFFT_HANDLE_R2C CUFFT_D2Z
#define CUFFT_HANDLE_C2R CUFFT_Z2D
#endif //ENABLE_SINGLE_PRECISION

/*---Functions: ---------------------------------------------------------------------------------------------------------------------------------------------------*/

void initialize_phase(PHASE *p, int argc, char **argv)
{
    int gpu_number = -1;
    //Read in file name:
    if(argc<2)
    {
        fprintf(stderr, "ERROR: No input file given.\n");
        exit(1);
    } else if(argc>3) 
    {
        fprintf(stderr, "ERROR: Too many parameters given.");
        exit(1);
    } else
    {
        sprintf(p->fname, "%s", argv[1]);
        if(argc==3)
            gpu_number = atoi(argv[2]);
    }


    print_copyright_notice();
    read_hdf5(p);
    setup_cuda(gpu_number);
    initialize_memory(p);
    init_parameters(p);
    init_random_states(p);//this needs some more attention what to take best as kernel and how many pcg states. 
    init_fft(p);
    generate_random_neighbor_permutation(p);

    print_setup(p);

}

void finalize_phase(PHASE *p)
{
    close_file(p);
    free_memory(p);
}

void setup_cuda(int gpu_number)
{
    int num_avail_devices;
    cudaGetDeviceCount(&num_avail_devices);
    if( gpu_number >= 0)
    {
        if( gpu_number < num_avail_devices )
        {
            cudaSetDevice(gpu_number);
        } else
        {
            printf("Given GPU# was higher than available. Resetting to last GPU.\n");
            cudaSetDevice(num_avail_devices-1);
        }
    } else
    {
        cudaSetDevice(num_avail_devices-1);
    }
}

void initialize_memory(PHASE *p)
{
    HANDLE_ERROR(cudaMalloc((void **)&p->_chiN, p->n_components*p->n_components*sizeof(udm_scalar)));

    HANDLE_ERROR(cudaMalloc((void **)&p->_rho, p->n_components*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_phi, p->Nfull*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_psi, p->Nfull*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_lap_psi, p->Nfull*sizeof(udm_scalar)));
#ifndef ENABLE_CONVERSIONS
    HANDLE_ERROR(cudaMalloc((void **)&p->_conv_psi, p->Nfull*sizeof(udm_scalar))); //actually just necessary for block copolymers.
#else //ENABLE_CONVERSIONS
    int n = p->n_components;
    if( p->n_conversions > p->n_components )
        n = p->n_conversions;
    HANDLE_ERROR(cudaMalloc((void **)&p->_conv_psi, n*p->Nreal*sizeof(udm_scalar))); //also used as buffer for conversions. 
#endif //ENABLE_CONVERSIONS
    HANDLE_ERROR(cudaMalloc((void **)&p->_dphi, p->Nreal*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_mu, p->Nfull*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_lap_mu, p->Nfull*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_fe, p->Nreal*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_gpu_buffer0, (p->Nfull + N_THREADS - 1)/N_THREADS*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_gpu_buffer1, (p->Nfull + N_THREADS - 1)/N_THREADS*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_neighbor_list, 3*NUM_NEIGHBORS_TOTAL*sizeof(int)));

    HANDLE_ERROR(cudaMalloc((void **)&p->_fphi, p->n_components * p->Ncomplex*sizeof(fft_complex)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_fpsi, p->n_components * p->Ncomplex*sizeof(fft_complex)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_fdphi, p->Ncomplex*sizeof(fft_complex)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_fconv, p->n_components * p->Ncomplex*sizeof(fft_complex)));
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY //not needed because these parts done with finite differences.
    HANDLE_ERROR(cudaMalloc((void **)&p->_fmu, p->n_components * p->Ncomplex*sizeof(fft_complex)));
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
#ifdef ENABLE_REFLECTING_BC 
    //HANDLE_ERROR(cudaMalloc((void **)&p->_fields_full, p->n_components*(2*p->N[0]-2)*p->N[1]*p->N[2]*sizeof(udm_scalar)));
    HANDLE_ERROR(cudaMalloc((void **)&p->_fields_full, 2*p->Nfull*sizeof(udm_scalar)));
#endif //ENABLE_REFLECTING_BC 

#ifdef ENABLE_EXTERNAL_FIELD
    if( p->external_field != NULL)
        HANDLE_ERROR(cudaMalloc((void **)&p->_external_field, p->Nfull*sizeof(udm_scalar)));
    else
        p->_external_field = NULL;
    if( p->wall != NULL)
        HANDLE_ERROR(cudaMalloc((void **)&p->_wall, p->Nreal*sizeof(uint8_t)));
    else
        p->_wall = NULL;
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if( p->umbrella_field != NULL)
        HANDLE_ERROR(cudaMalloc((void **)&p->_umbrella_field, p->Nfull*sizeof(udm_scalar)));
    else
        p->_umbrella_field = NULL;
#endif //ENABLE_UMBRELLA_FIELD
#ifdef ENABLE_NOISE
    HANDLE_ERROR(cudaMalloc((void **)&p->_noise, 3*p->Nfull*sizeof(udm_scalar)));
#endif //ENABLE_NOISE
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    HANDLE_ERROR(cudaMalloc((void **)&p->_mobility, p->Nfull*sizeof(udm_scalar)));
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    //=== Fourier field memory allocation===============================================================
    HANDLE_ERROR(cudaMalloc((void **)&p->_Kern, p->Ncomplex*sizeof(fft_complex))); 
    HANDLE_ERROR(cudaMalloc((void **)&p->_K2, p->Ncomplex*sizeof(fft_complex))); 
    HANDLE_ERROR(cudaMalloc((void **)&p->_Lin, p->n_components * p->Ncomplex*sizeof(fft_complex))); 
    //Component related memory: =========================================================================================================================================
    for(int i=0; i<p->n_components; i++)
    {
        COMPONENT *c = p->comps[i];
        c->_phi = p->_phi + i * p->Nreal;
        c->_psi = p->_psi + i * p->Nreal;
        c->_lap_psi = p->_lap_psi + i * p->Nreal;
        c->_conv_psi = p->_conv_psi + i * p->Nreal;
        c->_mu = p->_mu + i * p->Nreal;
        c->_lap_mu = p->_lap_mu + i * p->Nreal;
        c->_noise = p->_noise + i * 3 * p->Nreal;
        c->_fphi = p->_fphi + i * p->Ncomplex;
        c->_fpsi = p->_fpsi + i * p->Ncomplex;
        c->_fconv = p->_fconv + i * p->Ncomplex;
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
        c->_fmu = p->_fmu + i * p->Ncomplex;
#else //IF ENABLE_DENSITY_DEPENDENT_MOBILITY
        c->_mobility = p->_mobility + i * p->Nreal;
        HANDLE_ERROR(cudaMalloc((void **)&c->_ddm_parameters, (p->n_components+1)*sizeof(udm_scalar)));
        HANDLE_ERROR(cudaMemcpy(c->_ddm_parameters, c->ddm_parameters, (p->n_components+1)*sizeof(udm_scalar), cudaMemcpyHostToDevice));
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
#ifdef ENABLE_REFLECTING_BC
        c->_field_full = p->_fields_full + i * 2 * p->Nreal;
        //c->_field_full = p->_fields_full + i * (2*p->N[0]-2)*p->N[1]*p->N[2];
#endif //ENABLE_REFLECTING_BC
    }

    //Conversion related memory (custom domain)===================================================================================
    for(int i=0; i<p->n_conversions; i++)
    {
        if( p->convs[i]->custom_domain != NULL )
        {
            HANDLE_ERROR(cudaMalloc((void **)&p->convs[i]->_custom_domain, p->Nreal*sizeof(uint8_t)));
            HANDLE_ERROR(cudaMemcpy(p->convs[i]->_custom_domain, p->convs[i]->custom_domain, p->Nreal * sizeof(uint8_t), cudaMemcpyHostToDevice));
        } else
        {
            p->convs[i]->_custom_domain = NULL;
        }
    }


//Now copyin the fields, all parameters should now be present on device.
    HANDLE_ERROR(cudaMemcpy(p->_chiN, p->chiN, p->n_components*p->n_components*sizeof(udm_scalar), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(p->_phi, p->phi, p->Nfull * sizeof(udm_scalar), cudaMemcpyHostToDevice));
#ifdef ENABLE_EXTERNAL_FIELD
    if( p->_external_field != NULL)
        HANDLE_ERROR(cudaMemcpy(p->_external_field, p->external_field, p->Nfull * sizeof(udm_scalar), cudaMemcpyHostToDevice));
    if( p->_wall != NULL)
        HANDLE_ERROR(cudaMemcpy(p->_wall, p->wall, p->Nreal * sizeof(uint8_t), cudaMemcpyHostToDevice));
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if( p->umbrella_field != NULL)
        HANDLE_ERROR(cudaMemcpy(p->_umbrella_field, p->umbrella_field, p->Nfull * sizeof(udm_scalar), cudaMemcpyHostToDevice));
#endif //ENABLE_UMBRELLA_FIELD
    
}

void free_memory(PHASE *p)
{
    //free on host
    free(p->chiN);
    free(p->phi);
    free(p->mu);
    free(p->fe);
#ifdef ENABLE_EXTERNAL_FIELD
    if(p->external_field!=NULL)
        free(p->external_field);
    if(p->wall!=NULL)
        free(p->wall);
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if(p->umbrella_field!=NULL)
        free(p->umbrella_field);
#endif //ENABLE_EXTERNAL_FIELD
    for(int i=0; i<p->n_components; i++)
    {
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
        free(p->comps[i]->ddm_parameters);
        cudaFree(p->comps[i]->_ddm_parameters);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
        free(p->comps[i]);
    }
    free(p->comps);
#ifdef ENABLE_CONVERSIONS
    for(int i=0; i<p->n_conversions; i++)
    {
        free(p->convs[i]->reactants);
        free(p->convs[i]->r_ratios);
        free(p->convs[i]->products);
        free(p->convs[i]->p_ratios);
        free(p->convs[i]->custom_domain);
        cudaFree(p->convs[i]->_custom_domain);
        free(p->convs[i]);
    }
    free(p->convs);
#endif //ENABLE_CONVERSIONS

    //free on device
    cudaFree(p->_chiN);
    cudaFree(p->_phi);
    cudaFree(p->_psi);
    cudaFree(p->_lap_psi);
    cudaFree(p->_conv_psi);
    cudaFree(p->_dphi);
    cudaFree(p->_mu);
    cudaFree(p->_lap_mu);
    cudaFree(p->_fe);
    cudaFree(p->_gpu_buffer0);
    cudaFree(p->_gpu_buffer1);
    cudaFree(p->_neighbor_list);
    cudaFree(p->_fphi);
    cudaFree(p->_fpsi);
    cudaFree(p->_fdphi);
    cudaFree(p->_fconv);
#ifndef ENABLE_REFLECTING_BC
    cudaFree(p->_fmu);
#endif //ENABLE_REFLECTING_BC
#ifdef ENABLE_EXTERNAL_FIELD
    if(p->_external_field!=NULL)
        cudaFree(p->_external_field);
    if(p->_wall!=NULL)
        cudaFree(p->_wall);
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    if(p->_umbrella_field!=NULL)
        cudaFree(p->_umbrella_field);
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_NOISE
    cudaFree(p->_noise);
#endif //ENABLE_NOISE
#ifdef ENABLE_REFLECTING_BC
    cudaFree(p->_fields_full);
#endif //ENABLE_REFLECTING_BC
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    cudaFree(p->_mobility);
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
    cudaFree(p->_rand_states);
    cudaFree(p->_Kern);
    cudaFree(p->_K2);
    cudaFree(p->_Lin);

    cufftDestroy(p->plan_single_forward);
    cufftDestroy(p->plan_single_backward);
    cufftDestroy(p->plan_multiple_forward);
    cufftDestroy(p->plan_multiple_backward);
}

void init_parameters(PHASE *p)
{
    //blocks and threads for CUDA:
    p->threads.x = N_THREADS;
    p->threads.y = p->threads.z = 1;
    
    p->blocks_single.x = ( p->Nreal + N_THREADS - 1 )/N_THREADS;
    p->blocks_single_fs.x = ( p->Ncomplex + N_THREADS - 1 )/N_THREADS;
    p->blocks_multiple.x = ( p->Nfull + N_THREADS - 1 )/N_THREADS;
    p->blocks_multiple_fs.x = ( p->n_components*p->Ncomplex + N_THREADS - 1 )/N_THREADS;

    p->blocks_multiple.y = p->blocks_multiple.z = p->blocks_single.y = p->blocks_single.z = p->blocks_multiple_fs.y = p->blocks_multiple_fs.z = p->blocks_single_fs.y = p->blocks_single_fs.z = 1;

    p->blocks_slice.x = ( p->Nslice + N_THREADS - 1 )/N_THREADS;
    p->blocks_slice.y = p->blocks_slice.z = 1;
    
    //parameters:
    if( p->version >= 1 )
    {
        for(int i=0; i<p->n_components; i++)
        {
            if(p->comps[i]->arch==P1)
            {
                p->comps[i]->params[0] = 2.*p->comps[0]->N/((udm_scalar)p->comps[i]->N);
                p->comps[i]->params[1] = -1/3.;
                p->comps[i]->param_l0  = -1/6.;
            } else if(p->comps[i]->arch==P2)
            {
                udm_scalar soff = 1.572 - 2.702 * p->comps[i]->f * p->comps[p->comps[i]->bonded_comp_index]->f;
                p->comps[i]->params[0] = 0.5*p->comps[0]->N*(soff/p->comps[i]->f/(1-p->comps[i]->f) - 1/(1-p->comps[i]->f))/(1-p->comps[i]->f)/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[1] = 36/p->comps[i]->f*p->comps[0]->N*p->comps[0]->N/(udm_scalar)p->comps[i]->N/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[2] = -36/sqrt(p->comps[i]->f*(1-p->comps[i]->f))*p->comps[0]->N*p->comps[0]->N/(udm_scalar)p->comps[i]->N/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[3] = -p->comps[0]->N/sqrt(p->comps[i]->f*(1-p->comps[i]->f))/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[4] = -1/3.;
                p->comps[i]->param_l0  = -1/6.;

            }
        }
    } else if ( p->version == 0 ) {
        for(int i=0; i<p->n_components; i++)
        {
            if(p->comps[i]->arch==P1)
            {
                p->comps[i]->params[0] = 2./((udm_scalar)p->comps[i]->N);
                p->comps[i]->params[1] = -1/3./(udm_scalar)p->comps[0]->N;
                p->comps[i]->param_l0 = -1/6./(udm_scalar)p->comps[0]->N;
            } else if(p->comps[i]->arch==P2)
            {
                udm_scalar soff = 1.572 - 2.702 * p->comps[i]->f * p->comps[p->comps[i]->bonded_comp_index]->f;
                p->comps[i]->params[0] = 0.5*(soff/p->comps[i]->f/(1-p->comps[i]->f) - 1/(1-p->comps[i]->f))/(1-p->comps[i]->f)/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[1] = 36/p->comps[i]->f*p->comps[0]->N/(udm_scalar)p->comps[i]->N/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[2] = -36/sqrt(p->comps[i]->f*(1-p->comps[i]->f))*p->comps[0]->N/(udm_scalar)p->comps[i]->N/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[3] = -1/sqrt(p->comps[i]->f*(1-p->comps[i]->f))/(udm_scalar)p->comps[i]->N;
                p->comps[i]->params[4] = -1/3./(udm_scalar)p->comps[0]->N;
                p->comps[i]->param_l0 = -1/6./(udm_scalar)p->comps[0]->N;

            }
        }
    
    }
    //For Fourier fields allocate and initializae on host than copy-in.
    fft_real *Kern, *K2, *Lin;
    if ((Kern=   (fft_real*)malloc(p->Ncomplex*sizeof(fft_real)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory\n");
                exit(1);
        }
    if ((K2=   (fft_real*)malloc(p->Ncomplex*sizeof(fft_real)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory\n");
                exit(1);
        }
    if ((Lin=   (fft_real*)malloc(p->n_components*p->Ncomplex*sizeof(fft_real)))==NULL) {
                fprintf(stderr,"ERROR: Cannot allocate memory\n");
                exit(1);
        }


#ifndef ENABLE_REFLECTING_BC
    int N0full = p->N[0];
    double L0full = p->L[0];
#else //IF ENABLE_REFLECTING_BC
    //int N0full = 2*p->N[0]-2;
    int N0full = 2*p->N[0];
    double L0full = N0full * p->dr[0];
#endif //ENABLE_REFLECTING_BC
    unsigned int Nlogical = N0full * p->N[1] * p->N[2];
    switch(p->dim)
    {
        case 1:
            for(int i=0;i<(N0full/2+1);i++)
            {
                udm_scalar KZ               = 2*PI/L0full*i;
                Kern[i]              = 1/((KZ*KZ + 1/(p->cutoff * p->cutoff))) / (udm_scalar)Nlogical;
                K2[i]              = -(KZ*KZ) / (udm_scalar)Nlogical;
                for(int ci=0; ci<p->n_components; ci++)
                {
                    COMPONENT *c = p->comps[ci];
                    Lin[i+ci*p->Ncomplex] = 1/(1 - p->dt * c->m * c->param_l0 * K2[i] * Nlogical * K2[i] * Nlogical) / (udm_scalar)Nlogical;
                }
            } 
            break;
        case 2:
            for(int i=0;i<N0full;i++)
            {
                for(int j=0;j<(p->N[1]/2+1);j++)
                {
                    int l                   = i*(p->N[1]/2+1)+j;
                    udm_scalar KZ;
                    if(i<N0full/2)
                        KZ                  = 2*PI/L0full*i;
                    else
                        KZ                  = -2*PI/L0full*(N0full-i);
                    udm_scalar KY               = 2*PI/p->L[1]*j;
                    Kern[l]              = 1/((KY*KY+KZ*KZ) + 1/(p->cutoff * p->cutoff)) / (udm_scalar)Nlogical;
                    K2[l]                = -(KY*KY+KZ*KZ) / (udm_scalar)Nlogical;
                    for(int ci=0; ci<p->n_components; ci++)
                    {
                        COMPONENT *c = p->comps[ci];
                        Lin[l+ci*p->Ncomplex] = 1/(1 - p->dt * c->m * c->param_l0 * K2[l] * Nlogical * K2[l] * Nlogical) / (udm_scalar)Nlogical;
                    }
                } 
            }
            break;
        case 3:;
            for(int i=0;i<N0full;i++){
                for(int j=0;j<p->N[1];j++){
                    for(int k=0;k<(p->N[2]/2+1);k++){
                        int l                       = i*p->N[1]*(p->N[2]/2+1)+j*(p->N[2]/2+1)+k;
                        udm_scalar KZ;
                        if(i<N0full/2)
                            KZ                  = 2*PI/L0full*i;
                        else
                            KZ                  = -2*PI/L0full*(N0full-i);
                        udm_scalar KY;
                        if(j<p->N[1]/2)
                            KY                  = 2*PI/p->L[1]*j;
                        else
                            KY                  = -2*PI/p->L[1]*(p->N[1]-j);
                        udm_scalar KX               = 2*PI/p->L[2]*k;
                        Kern[l]              = 1/((KX*KX+KY*KY+KZ*KZ) + 1/(p->cutoff * p->cutoff)) / (udm_scalar)Nlogical;
                        K2[l]                = -(KX*KX+KY*KY+KZ*KZ) / (udm_scalar)Nlogical;
                        for(int ci=0; ci<p->n_components; ci++)
                        {
                            COMPONENT *c = p->comps[ci];
                            Lin[l+ci*p->Ncomplex] = 1/(1 - p->dt * c->m * c->param_l0 * K2[l] * Nlogical * K2[l] * Nlogical) / (udm_scalar)Nlogical;
                        }
                    } 
                }
            }
            break;
        default:;
    }
    //behavior at q=(0,0,0):
    Kern[0] = 0.;
    for(int ci=0; ci<p->n_components; ci++)
    {
        Lin[ci*p->Ncomplex] = 0;//to enforce zero mean in concentration change
    }
    HANDLE_ERROR(cudaMemcpy(p->_Kern, Kern, p->Ncomplex * sizeof(fft_real), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(p->_K2, K2, p->Ncomplex * sizeof(fft_real), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(p->_Lin, Lin, p->n_components*p->Ncomplex * sizeof(fft_real), cudaMemcpyHostToDevice));
    free(Kern);
    free(K2);
    free(Lin);
}
    
void init_fft(PHASE *p)
{
#ifndef ENABLE_REFLECTING_BC
    int N[3] = {p->N[0], p->N[1], p->N[2]};
#else //IF ENABLE_REFLECTING_BC
    int N[3] = {2*p->N[0], p->N[1], p->N[2]};
    //int N[3] = {(2*p->N[0]-2), p->N[1], p->N[2]};
#endif //IFENABLE_REFLECTING_BC

    //Set up fftw plans:
    HANDLE_ERROR(cufftPlanMany(&p->plan_multiple_forward, p->dim, N, NULL, 1, 0, NULL, 1, 0, CUFFT_HANDLE_R2C, p->n_components));
    HANDLE_ERROR(cufftPlanMany(&p->plan_multiple_backward, p->dim, N, NULL, 1, 0, NULL, 1, 0, CUFFT_HANDLE_C2R, p->n_components));
    HANDLE_ERROR(cufftPlanMany(&p->plan_single_forward, p->dim, N, NULL, 1, 0, NULL, 1, 0, CUFFT_HANDLE_R2C, 1));
    HANDLE_ERROR(cufftPlanMany(&p->plan_single_backward, p->dim, N, NULL, 1, 0, NULL, 1, 0, CUFFT_HANDLE_C2R, 1));
}

void generate_random_neighbor_permutation(PHASE *p)
{
    int neigh[NUM_NEIGHBORS_TOTAL]; 

    //generate uniform neighbor list
    for(int i=0; i<NUM_NEIGHBORS_TOTAL; i++)
    {
        neigh[i] = i;
    }
    //generate permutated neighbor list with 1 index (0 to NUM_NEIGHBORS_TOTAL-1)
    for(int i=NUM_NEIGHBORS_TOTAL-1; i>=0; i--)
    {
    //generate a random number [0, i] --> all (so far) unchagned numbers 
    int j = rand() % (i+1);

    //swap the last element with element at random index
    int temp = neigh[i];
    neigh[i] = neigh[j];
    neigh[j] = temp;
    }
    //calculate x,y,z indices
    int *neighbor_list;
    if ((neighbor_list = (int*)malloc(3*NUM_NEIGHBORS_TOTAL*sizeof(int)))==NULL) {
        fprintf(stderr,"ERROR: Cannot allocate memory\n");
        exit(1);
    }
    int num_neighbors_per_dim = 2*NUM_NEIGHBORS+1;
    for(int i=0; i<NUM_NEIGHBORS_TOTAL; i++)
    {
        int z = neigh[i]/(num_neighbors_per_dim*num_neighbors_per_dim); //value between 0 and num_neighbors_per_dim-1
        int res = neigh[i] - z*num_neighbors_per_dim*num_neighbors_per_dim;
        int y = res/num_neighbors_per_dim;//value between 0 and num_neighbors_per_dim-1
        int x = res - y*num_neighbors_per_dim;//value between 0 and num_neighbors_per_dim-1

        neighbor_list[3*i  ] = z - NUM_NEIGHBORS;
        neighbor_list[3*i+1] = y - NUM_NEIGHBORS;
        neighbor_list[3*i+2] = x - NUM_NEIGHBORS;
    }
    HANDLE_ERROR(cudaMemcpy(p->_neighbor_list, neighbor_list, 3*NUM_NEIGHBORS_TOTAL*sizeof(int), cudaMemcpyHostToDevice));
    free(neighbor_list);
}
