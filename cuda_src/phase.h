/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PHASE_H
#define PHASE_H

#include <stdlib.h>
#include <hdf5.h>
#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cufft.h>

#ifdef ENABLE_SINGLE_PRECISION
typedef float udm_scalar;

typedef cufftComplex fft_complex;
typedef cufftReal    fft_real;

#else //ENABLE_SINGLE_PRECISION
typedef double udm_scalar;

typedef cufftDoubleComplex fft_complex;
typedef cufftDoubleReal    fft_real;
#endif //ENABLE_SINGLE_PRECISION

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
#define HANDLE_RESULT( err ) (HandleResult( err, __FILE__, __LINE__ ))

/** number of neighbors in each direction and dimension i.e 9 neighbors in each dimension (including self)
 * Accordingly, in 3D there are 9^3 = 729 total neighbors. This is used for the regularization. 
 */
#define NUM_NEIGHBORS 4 
#define NUM_NEIGHBORS_TOTAL 729

/*---Structures and Enum Definitions: ---------------------------------------------------------------------------------------------------------------------------------------------------*/
/** List of possible polymer architectures. At the moment only contains solvents/homopoly-
 * mers (1 block/component per molecule) and diblock copolymers (2 blocks/components per 
 * molecule). 
 */
typedef enum PolyArch 
{ //Polymer Architecture AND number of blocks and thus order parameter fields for this species.
    P1 = 1,//Homopolymer or Solvent
    P2 = 2//Diblock copolymer
}PolyArch;

/** List of posible conversion types:
 *      0 -- global: conversion performed everywhere
 *      1 -- local_left: conversion performed only in first slab (of first/z-dimension)
 *      2 -- local_right: conversion performed only in last slab (of first/z-dimension)
 *     -1 -- custom: conversion performed according to conv->_custom_domain which is read
 *                   from the input file.
 * NOTE: the options transport_left/right are depricated and will be removed in the
 *       future.
 */
typedef enum ConvType
{
    custom=-1, 
    global=0, 
    local_left=1, 
    local_right=2,
    transport_left=3, 
    transport_right=4
} ConvType;

/** Structure for each component with all parameters and memory.
 */
typedef struct COMPONENT 
{
    int index;

    PolyArch arch;

    int N; //polymer length (including all blocks)
    udm_scalar f; //Composition (ONLY for arch=P2)
    udm_scalar rho; //average overall density
#ifdef ENABLE_EVAPORATION_STABILIZATION
    udm_scalar rhobulk; //to enforce dirichlet bc in the comoving frame. 
#endif //ENABLE_EVAPORATION_STABILIZATION
    //udm_scalar drho; //for stabilization, where density surpasses zero, the drho is shared to all other gridpoints. 
    udm_scalar m; //monomer mobility scale (constant), unused for density-dependent mobility
#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    udm_scalar *_mobility;
    int ddm_activated;
    udm_scalar *ddm_parameters;
    udm_scalar *_ddm_parameters;

#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY

    int bonded_comp_index;

    //Real space variable fields:
    udm_scalar *phi;
    udm_scalar *_phi;
    udm_scalar *_psi; 
    udm_scalar *_lap_psi;
    udm_scalar *_conv_psi;
    udm_scalar *_mu; 
    udm_scalar *_lap_mu;
#ifdef ENABLE_REFLECTING_BC
    udm_scalar *_field_full;
#endif //ENABLE_REFLECTING_BC

    udm_scalar *_noise;

    //Fourier space variable fields:
    fft_complex         *_fphi;
    fft_complex         *_fpsi;
    fft_complex         *_fconv;
    fft_complex         *_fmu;

    //Parameters for evaluation of chemical potential mu:
    udm_scalar params[5];
    udm_scalar param_l0;
} COMPONENT;

/** Structure for each defined conversion with relevant parameters and memory.
 */
typedef struct CONVERSION
{
    int index;
    ConvType type;

    int n_reactants;
    COMPONENT **reactants;
    udm_scalar *r_ratios;
    int n_products;
    COMPONENT **products;
    udm_scalar *p_ratios;

    udm_scalar reaction_rate;
    udm_scalar factor;

    udm_scalar *buffer;
    uint8_t *custom_domain;
    uint8_t *_custom_domain;
} CONVERSION;
    
/** Main simulation structure that contains all parameters and (pointers to) all memory
 * including all components (p->comps) and all convesions (p->convs). As given by CUDA
 * standard notation pointers to device memory start with underscore `_`.
 */
typedef struct PHASE 
{ //almost everything will be hosted on host, only fields on device, parameters will be given to the kernels.
    int version;
    int dim;
    int n_components;
    int n_conversions;

    COMPONENT **comps; // Array of pointers to all components.
    CONVERSION **convs; //Array of pointers to all conversions.

    udm_scalar *chiN; //matrix of interaction parameters. 
    udm_scalar *_chiN; //matrix of interaction parameters on device 
    udm_scalar *_rho; //mean density on device. 

    udm_scalar dt;
    udm_scalar T;
    int  N[3];//Logical number of grid points in z dir is 2*N[2].
    long long Nt;
    udm_scalar Tsave;
    int tsave;
    udm_scalar L[3];
    udm_scalar dr[3];
    udm_scalar Tstart;

    //noise parameters:
    udm_scalar effective_temp;
    udm_scalar sqrtnbar;
    udm_scalar noise_prefactor;

    int  Nreal;
    int  Ncomplex;
    int  Nslice;
    int  Nfull;

    udm_scalar          oodz32, oody32, oodx32;
    udm_scalar          oodz2, oody2, oodx2;

    udm_scalar *phi; //Flattened array of all phi fields. 
    udm_scalar *_phi; //Flattened array of all phi fields on device. 
    udm_scalar *_psi;  
    udm_scalar *_lap_psi;  
    udm_scalar *_conv_psi;  
    udm_scalar *mu; //in case of write-out also need this on host.
    udm_scalar *_mu;
    udm_scalar *_lap_mu;
    udm_scalar *fe;
    udm_scalar *_fe;
    udm_scalar *_dphi;
    udm_scalar Fe;
    fft_complex *_fdphi;
    fft_complex *_fphi;
    fft_complex *_fpsi;
    fft_complex *_fconv;
    fft_complex *_fmu;
#ifdef ENABLE_REFLECTING_BC
    udm_scalar *_fields_full;//ncomps*2Nz*Ny*Nz-sized fields (with variable meaning)
#ifndef ENABLE_DENSITY_DEPENDENT_MOBILITY
#error "Option ENABLE_REFLECTING_BC requires also option ENABLE_DENSITY_DEPENDENT_MOBILITY"
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY
#endif //ENABLE_REFLECTING_BC

#ifdef ENABLE_DENSITY_DEPENDENT_MOBILITY
    udm_scalar *_mobility;
    int gs_iterations;
#endif //ENABLE_DENSITY_DEPENDENT_MOBILITY

#ifdef ENABLE_EXTERNAL_FIELD
    udm_scalar *external_field;
    udm_scalar *_external_field;
    uint8_t *wall;
    uint8_t *_wall;
#endif //ENABLE_EXTERNAL_FIELD
#ifdef ENABLE_UMBRELLA_FIELD
    udm_scalar *umbrella_field;
    udm_scalar *_umbrella_field;
    udm_scalar umbrella_kappa;
#endif //ENABLE_UMBRELLA_FIELD
    udm_scalar *_noise;

#ifdef ENABLE_EVAPORATION_STABILIZATION
    int conversion_delay;
    int interface_position;
    int shift;
#endif //ENABLE_EVAPORATION_STABILIZATION

    //Fourier space constant fields:
    udm_scalar         cutoff; //Lengthscale at which the kernel cuts off.
    fft_real *_Kern;
    fft_real *_K2;
    fft_real *_Lin;

    //CUDA blocks and threads:
    dim3 blocks_single; //single field
    dim3 blocks_multiple; //n_component fields
    dim3 blocks_single_fs; //single field in fourier space
    dim3 blocks_multiple_fs; //...
    dim3 threads; //threads constant for all.
    dim3 blocks_slice; //just for one slice, e.g. local conversions.

    //FFTW plans:
    cufftHandle plan_single_forward;
    cufftHandle plan_single_backward;
    cufftHandle plan_multiple_forward;
    cufftHandle plan_multiple_backward;

    //Random_number states on device:
    curandState_t *_rand_states;

    //for reduction operations on GPU, provide memory:
    udm_scalar *_gpu_buffer0;
    udm_scalar *_gpu_buffer1;

    //neighbor list for regularization:
    int *_neighbor_list;

    //hdf5 saving variables:
    char fname[150];
    hid_t file, dset_phi, dspace, mspace;
    hid_t chunk_prop;
    hsize_t start[5], count[5];
} PHASE;

/*---Function Declarations: ---------------------------------------------------------------------------------------------------------------------------------------------------*/
/** Screen output for each component.
 *
 * \param c Pointer to a specific struct of a component (COMPONENT).
 */
void print_component_setup(COMPONENT *c);

/** Screen output for each conversion.
 *
 * \param c Pointer to a specific struct of a conversion (CONVERSION).
 */
void print_conversion_setup(CONVERSION *c);

/** General screen output at start of simulation.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 */
void print_setup(PHASE *p);

/** Screen output for copyright notice at start of simulation.
 */
void print_copyright_notice();

/** Initialize COMPONENT struct with given parameters.
 *
 * \param p Pointer to fully intitialized simulation structure (PHASE).
 * \param comp_index Index of component (order of p->comp array)
 * \param arch PolyArch index of the polymer architecture this component belongs to
 * \param f Block ratio of this component in the polymer (1 for P1, 0<f<1 for P2)
 * \param rho Mean concentration of this component.
 * \param m Mobility lambda_c in units of lambda_0 of this component.
 * \param corr_comp_index Index of the connected block. Only initialized for copolymer (P2).
 */
void set_up_component(PHASE *p, int comp_index, PolyArch arch, int N, udm_scalar f, udm_scalar rho, udm_scalar m, int corr_comp_index);

#endif //PHASE_H
