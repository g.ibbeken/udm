/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <time.h>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <curand.h>
#include <curand_kernel.h>
#include <cuComplex.h>

#include "phase.h"


/*---Functions: -------------------------------------------------------------------------------------------------------------------------------------------*/
__device__ __host__ unsigned int index_from_indices(int z, int y, int x, int Nz, int Ny, int Nx)
{
#ifndef ENABLE_REFLECTING_BC
    z = (z+(NUM_NEIGHBORS+1)*Nz)%Nz; //+1 just to make sure, compiler should optimize ( but only relevant for 1d, 2d cases)
#else //IF ENABLE_REFLECTING_BC
    if( z < 0 )
        z = -z-1;
        //z = -z;
    else if( z >= Nz )
        z = 2*Nz-z-1;
        //z = 2*Nz-z-2;
#endif //ENABLE_REFLECTING_BC
    y = (y+(NUM_NEIGHBORS+1)*Ny)%Ny;
    x = (x+(NUM_NEIGHBORS+1)*Nx)%Nx;
    unsigned int index = (z * Ny + y) * Nx +  x;
    return index;
}

__device__ __host__ void indices_from_index(unsigned int index, int *z, int *y, int *x, int Nz, int Ny, int Nx)
{
    unsigned int Nslice = Ny * Nx;
    *z       = (int)(index/Nslice);
    unsigned int remaining_index = index%Nslice;
    *y       = (int)(remaining_index/Nx);
    *x       = (int)(remaining_index%Nx);
}

__device__ udm_scalar atomicExch_dp(double *address, double val) //float counterpart exists in cuda
{
    unsigned long long int *address_as_ull = (unsigned long long int *)address; 
    unsigned long long int previous = atomicExch(address_as_ull, __double_as_longlong(val));
    return __longlong_as_double(previous);
}

__device__ void take_from_neighbors(udm_scalar *phi, int i, int Nz, int Ny, int Nx, int *neighbor_list) //case phi[i] < 0 
{   
    int z, y, x;
    indices_from_index(i, &z, &y, &x, Nz, Ny, Nx);
#ifdef ENABLE_SINGLE_PRECISION
    udm_scalar accumulate = -atomicExch(&phi[i], 0.);

    int start_count = (*((int*)&accumulate))%NUM_NEIGHBORS_TOTAL; //interprete this value as integer (pseudo-random start value to loop through permutated neighbor list)
    int count = start_count;

    do
    {
        // neighbor
        int dz = neighbor_list[3*count  ];
        int dy = neighbor_list[3*count+1];
        int dx = neighbor_list[3*count+2];
        int neighbor = index_from_indices(z+dz, y+dy, x+dx, Nz, Ny, Nx);
        bool continue_reading = true;

        //Making use of atomics, we read then do the approporiate action with a check for change.
        int *phi_neighbor_as_int = (int *)&phi[neighbor];
        while(continue_reading)
        {
            int phi_neighbor_as_int_value = *phi_neighbor_as_int;
            udm_scalar phi_neighbor = __int_as_float(phi_neighbor_as_int_value);
            if( phi_neighbor > accumulate )
            {   //take if value has not changed.
                if( phi_neighbor_as_int_value == atomicCAS(phi_neighbor_as_int, phi_neighbor_as_int_value, __float_as_int(phi_neighbor - accumulate)) )
                {
                    //printf("At index %d, accumulate %.6e: try neighbor %d (-->%d) => done.\n", i, accumulate, dz, neighbor);
                    accumulate = 0;
                    continue_reading = false;
                }
            } else if( phi_neighbor > 0)
            {   //set accumulate to whats left to accumulate.
                if( phi_neighbor_as_int_value == atomicCAS(phi_neighbor_as_int, phi_neighbor_as_int_value, __float_as_int(0.)) )
                {
                    accumulate -= phi_neighbor;
                    continue_reading = false;
                    //printf("At index %d, accumulate %.6e: try neighbor %d (-->%d) => left %.6e.\n", i, phi_neighbor, dz, neighbor, accumulate);
                }
            } else
            {
                continue_reading = false;
                //printf("At index %d, accumulate %.6e: try neighbor %d (-->%d) => try next neighbor. Count %d started at %d\n", i, accumulate, dz, neighbor, count, start_count);
            }
        }
        count = (count+1)%NUM_NEIGHBORS_TOTAL;
    } while(accumulate > 0 && count != start_count);
#else   //ENABLE_SINGLE_PRECISION
    udm_scalar accumulate = -atomicExch_dp(&phi[i], 0.);

    int start_count = (int)(*((unsigned long long int*)&accumulate))%NUM_NEIGHBORS_TOTAL; //interprete this value as integer (pseudo-random start value to loop through permutated neighbor list)
    int count = start_count;

    do
    {
        // neighbor
        int dz = neighbor_list[3*count  ];
        int dy = neighbor_list[3*count+1];
        int dx = neighbor_list[3*count+2];
        int neighbor = index_from_indices(z+dz, y+dy, x+dx, Nz, Ny, Nx);
        bool continue_reading = true;

        //Making use of atomics, we read then do the approporiate action with a check for change.
        unsigned long long int *phi_neighbor_as_ull = (unsigned long long int *)&phi[neighbor];
        while(continue_reading)
        {
            unsigned long long int phi_neighbor_as_ull_value = *phi_neighbor_as_ull;
            udm_scalar phi_neighbor = __longlong_as_double(phi_neighbor_as_ull_value);
            if( phi_neighbor > accumulate )
            {   //take if value has not changed.
                if( phi_neighbor_as_ull_value == atomicCAS(phi_neighbor_as_ull, phi_neighbor_as_ull_value, __double_as_longlong(phi_neighbor - accumulate)) )
                {
                    accumulate = 0;
                    continue_reading = false;
                }
            } else if( phi_neighbor > 0)
            {   //set accumulate to whats left to accumulate.
                if( phi_neighbor_as_ull_value == atomicCAS(phi_neighbor_as_ull, phi_neighbor_as_ull_value, __double_as_longlong(0.)) )
                {
                    accumulate -= phi_neighbor;
                    continue_reading = false;
                }
            } else
            {
                continue_reading = false;
            }
        }
        count++;
    } while(accumulate > 0 && count != start_count);
#endif   //ENABLE_SINGLE_PRECISION
    //if( accumulate > 1e-4 )
    //    printf("Did not manage to accumlate enough material: missing %e at (%02d %02d %02d).\n", accumulate, z, y, x);
}

__global__ void multiply_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] = factor0[i] * factor1[i];
}

__global__ void multiply_repeat_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal, int Nrepeat)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] = factor0[i] * factor1[i%Nrepeat];
}

__global__ void multiply_kernel1(udm_scalar *factor0, udm_scalar *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        factor0[i] *= factor1[i];
}

__global__ void multiply_kernel1_uint8(udm_scalar *factor0, uint8_t *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        factor0[i] *= (udm_scalar) factor1[i];
}

__global__ void multiply_repeat_kernel1(udm_scalar *factor0, udm_scalar *factor1, int Ntotal, int Nrepeat)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        factor0[i] *= factor1[i%Nrepeat];
}

__global__ void multiply_kernel_complex(fft_complex *result, fft_complex *factor0, fft_real *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
    {
        result[i].x = factor0[i].x * factor1[i];
        result[i].y = factor0[i].y * factor1[i];
//#ifndef ENABLE_REFLECTING_BC
//#else //ENABLE_REFLECTING_BC
//        result[i].y = 0;
//#endif //ENABLE_REFLECTING_BC
    }
}

__global__ void multiply_repeat_kernel_complex(fft_complex *result, fft_complex *factor0, fft_real *factor1, int Ntotal, int Nrepeat)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
    {
        result[i].x = factor0[i].x * factor1[i%Nrepeat];
        result[i].y = factor0[i].y * factor1[i%Nrepeat];
//#ifndef ENABLE_REFLECTING_BC
//#else //ENABLE_REFLECTING_BC
//        result[i].y = 0;
//#endif //ENABLE_REFLECTING_BC
    }
}

__global__ void multiply_value_kernel1_complex(fft_complex *factor0, fft_real factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
    {
        factor0[i].x = factor0[i].x * factor1;
        factor0[i].y = factor0[i].y * factor1;
//#ifndef ENABLE_REFLECTING_BC
//#else //ENABLE_REFLECTING_BC
//        factor0[i].y = 0;
//#endif //ENABLE_REFLECTING_BC
    }
}

__global__ void multiply_kernel1_complex(fft_complex *factor0, fft_real *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
    {
        factor0[i].x = factor0[i].x * factor1[i];
        factor0[i].y = factor0[i].y * factor1[i];
//#ifndef ENABLE_REFLECTING_BC
//#else //ENABLE_REFLECTING_BC
//        factor0[i].y = 0;
//#endif //ENABLE_REFLECTING_BC
    }
}

__global__ void multiply_repeat_kernel1_complex(fft_complex *factor0, fft_real *factor1, int Ntotal, int Nrepeat)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
    {
        factor0[i].x = factor0[i].x * factor1[i%Nrepeat];
        factor0[i].y = factor0[i].y * factor1[i%Nrepeat];
//#ifndef ENABLE_REFLECTING_BC
//#else //ENABLE_REFLECTING_BC
//        factor0[i].y = 0;
//#endif //ENABLE_REFLECTING_BC
    }
}

__global__ void add_kernel(udm_scalar *result, udm_scalar *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] += factor1[i];
}

__global__ void add_and_multiply_kernel1(udm_scalar *result, udm_scalar factor0, udm_scalar *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] += factor0 * factor1[i];
}

__global__ void add_and_multiply_kernel2(udm_scalar *result, udm_scalar factor0, udm_scalar *factor1, udm_scalar *factor2, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] += factor0 * factor1[i] * factor2[i];
}

__global__ void sqrt_kernel(udm_scalar *result, udm_scalar *argument, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] = sqrt(argument[i]);
}

__global__ void sqrt_of_product_kernel(udm_scalar *result, udm_scalar *factor0, udm_scalar *factor1, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] = sqrt(factor0[i]*factor1[i]);
}

__global__ void copy_kernel(udm_scalar *result, udm_scalar *argument, int Ntotal)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Ntotal )
        result[i] = argument[i];
}

__global__ void copy_symmetric_field_kernel(udm_scalar *full_field, udm_scalar *field, int Nz, int Ny, int Nx)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nz*Ny*Nx )
    {
        const int Nslice = Ny * Nx;
        int Nzfull = 2*Nz;
        int z = Nzfull-1-(i/Nslice);
        //int Nzfull = 2*Nz-2;
        //int z = Nzfull-(i/Nslice);
        const int xy = i%Nslice;
        const int j = z * Nslice + xy;
        full_field[i] = field[i];
        if( z < Nzfull )
        {
            full_field[j] = field[i];
        }
    }
}

__global__ void density_variation_kernel(udm_scalar *dphi, udm_scalar *phi, int Nreal, int n_components)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nreal )
    {
        dphi[i] = -1;
        for(int c=0; c<n_components; c++)
        {
            int ic = i + c*Nreal;
            dphi[i] += phi[ic];
        }
    }
}

__global__ void density_variation_kernel(udm_scalar *dphi, udm_scalar *phi, uint8_t *wall, int Nreal, int n_components)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nreal )
    {
        if( wall[i] == 1 )
        {
            dphi[i] = 0.;
            for(int c=0; c<n_components; c++)
            {
                int ic = i + c*Nreal;
                dphi[i] += 0.25*phi[ic] ;
            }
        } else {
            dphi[i] = -1.;
            //dphi[i] = (udm_scalar)wall[i]-1;
            for(int c=0; c<n_components; c++)
            {
                int ic = i + c*Nreal;
                dphi[i] += phi[ic] ;
            }
        }
    }
}

__global__ void log_term_kernel(udm_scalar *mu, udm_scalar *psi, int Nreal, udm_scalar factor)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nreal )
    {
        if( psi[i] > 0 )
            mu[i] = factor * psi[i] * (2 * log(psi[i]) + 1);
        else
            mu[i] = 0;
    }
}

__global__ void umbrella_contribution_kernel(udm_scalar *mu, udm_scalar *psi, udm_scalar *umbrella_field, int Nfull, udm_scalar factor)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nfull )
    {
        mu[i] += factor * psi[i] * (psi[i]*psi[i] - umbrella_field[i]);
    }
}

__global__ void add_noise_term_kernel(udm_scalar *to_add, udm_scalar factor, udm_scalar *psi, udm_scalar *noise, udm_scalar oodz32, udm_scalar oody32, udm_scalar oodx32, int Nz, int Ny, int Nx)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int Nreal = Nz*Ny*Nx;
    if( i < Nreal )
    {
        int z, y, x;
        indices_from_index(i, &z, &y, &x, Nz, Ny, Nx);
        int m1 = index_from_indices(z-1, y, x, Nz, Ny, Nx);
        int p1 = index_from_indices(z+1, y, x, Nz, Ny, Nx);
        to_add[i] += factor * oodz32 * ((psi[p1] + psi[i]) * noise[p1] - (psi[m1] + psi[i]) * noise[i]); //psi[p1] + psi[i] gives average with factor 2,
        m1 = index_from_indices(z, y-1, x, Nz, Ny, Nx);                                                //which gives the 2 that should be in variable factor.
        p1 = index_from_indices(z, y+1, x, Nz, Ny, Nx);
        to_add[i] += factor * oody32 * ((psi[p1] + psi[i]) * noise[p1+Nreal] - (psi[m1] + psi[i]) * noise[i+Nreal]);
        m1 = index_from_indices(z, y, x-1, Nz, Ny, Nx);
        p1 = index_from_indices(z, y, x+1, Nz, Ny, Nx);
        to_add[i] += factor * oodx32 * ((psi[p1] + psi[i]) * noise[p1+2*Nreal] - (psi[m1] + psi[i]) * noise[i+2*Nreal]);
    }
}

__global__ void regularize_smaller_zero_kernel(udm_scalar *phi, int Nz, int Ny, int Nx, int Nfull, int *neighbor_list)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if( id < Nfull )
    {
            if( phi[id] < 0 )
            {
                int Nreal = Nz * Ny * Nx;
                int c = id/Nreal;
                int i = id-c*Nreal;
                take_from_neighbors(phi+c*Nreal, i, Nz, Ny, Nx, neighbor_list);
            }
    }
}

__global__ void initialize_value_kernel(udm_scalar *field, udm_scalar value, int Nreal)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if( id < Nreal )
    {
        field[id] = value;
    }
}

__global__ void initialize_value_kernel(uint8_t *field, uint8_t value, int Nreal)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if( id < Nreal )
    {
        field[id] = value;
    }
}

__global__ void add_homopolymer_contribution_free_energy_kernel(udm_scalar *fe, udm_scalar *phi, udm_scalar *lap, int ci, int Nz, int Ny, int Nx, udm_scalar dz, udm_scalar dy, udm_scalar dx, int n_components, udm_scalar param0, udm_scalar param1, udm_scalar *chiN)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int Nreal = Nz * Ny * Nx;
    udm_scalar *thisphi = phi + ci * Nreal;
    udm_scalar *thislap = lap + ci * Nreal;
    if( i < Nreal )
    {
                if(thisphi[i] != 0)
                    fe[i] += 0.5 * param0 * thisphi[i] * log(thisphi[i]);
                fe[i] += 0.5 * param1 * sqrt(thisphi[i]) * thislap[i];
                for(int ci2=0; ci2<n_components; ci2++)
                {
                    fe[i] += chiN[ci*n_components + ci2]/2. * thisphi[i] * phi[i+ci2*Nreal];
                }

    }
}

__global__ void add_diblock_copolymer_contribution_free_energy_kernel(udm_scalar *fe, udm_scalar *phi, udm_scalar *conv, udm_scalar *lap, int ci, int cibonded, int Nz, int Ny, int Nx, udm_scalar dz, udm_scalar dy, udm_scalar dx, int n_components, udm_scalar param0, udm_scalar param1, udm_scalar param2, udm_scalar param3, udm_scalar param4, udm_scalar *chiN)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int Nreal = Nz * Ny * Nx;
    udm_scalar *thisphi = phi + ci * Nreal;
    udm_scalar *thisconv = conv + ci * Nreal;
    udm_scalar *thislap = lap + ci * Nreal;
    if( i < Nreal )
    {
                if(thisphi[i] != 0)
                    fe[i] += 0.5 * param0 * thisphi[i] * log(thisphi[i]);
                fe[i] += 0.5 * param1 * thisconv[i] * sqrt(thisphi[i]);
                fe[i] += 0.5 * param2 * thisconv[i] * sqrt(phi[i+cibonded*Nreal]);
                fe[i] += 0.5 * param3 * sqrt(thisphi[i]) * sqrt(phi[i+cibonded*Nreal]);
                fe[i] += 0.5 * param4 * sqrt(thisphi[i]) * thislap[i];
                for(int ci2=0; ci2<n_components; ci2++)
                {
                    fe[i] += chiN[ci*n_components + ci2]/2. * thisphi[i] * phi[i+ci2*Nreal];
                }
    }
}

__global__ void compute_laplacian_kernel(udm_scalar *laplacian, udm_scalar *field, int Nreal, int Nz, int Ny, int Nx, udm_scalar oodz2, udm_scalar oody2, udm_scalar oodx2)
{
    const int ijk = blockIdx.x * blockDim.x + threadIdx.x;
    if( ijk < Nreal )
    {
        int z, y, x;
        indices_from_index(ijk, &z, &y, &x, Nz, Ny, Nx);
        const int ip1jk = index_from_indices(z+1, y,   x  , Nz, Ny, Nx);
        const int im1jk = index_from_indices(z-1, y,   x  , Nz, Ny, Nx);
        const int ijp1k = index_from_indices(z,   y+1, x  , Nz, Ny, Nx);
        const int ijm1k = index_from_indices(z,   y-1, x  , Nz, Ny, Nx);
        const int ijkp1 = index_from_indices(z,   y  , x+1, Nz, Ny, Nx);
        const int ijkm1 = index_from_indices(z,   y  , x-1, Nz, Ny, Nx);
        laplacian[ijk] = oodz2*(field[ip1jk]-2*field[ijk]+field[im1jk])
                       + oody2*(field[ijp1k]-2*field[ijk]+field[ijm1k])
                       + oodx2*(field[ijkp1]-2*field[ijk]+field[ijkm1]);

    }
}

__global__ void compute_dtgamma_kernel(udm_scalar *field, udm_scalar factor, udm_scalar *mobility, udm_scalar *psi, udm_scalar *mu, int Nreal, int Nz, int Ny, int Nx, udm_scalar oodz2, udm_scalar oody2, udm_scalar oodx2)
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nreal )
    {
        int z, y, x;
        indices_from_index(i, &z, &y, &x, Nz, Ny, Nx);
        int ip1 = index_from_indices(z+1, y, x, Nz, Ny, Nx);
        int im1 = index_from_indices(z-1, y, x, Nz, Ny, Nx);

        //prefactor dt/4 calculated on cpu and given to kernel, 
        udm_scalar prefactor = factor*oodz2;

        field[i]  =  prefactor * (mobility[ip1]*psi[ip1]*(mu[ip1]-mu[i]) + mobility[im1]*psi[im1]*(mu[im1]-mu[i]) + mobility[i]*psi[i]*(mu[im1]-2*mu[i]+mu[ip1]));
        field[i] += -prefactor * (mobility[ip1]*mu[ip1]*(psi[ip1]-psi[i]) + mobility[im1]*mu[im1]*(psi[im1]-psi[i]) + mobility[i]*mu[i]*(psi[im1]-2*psi[i]+psi[ip1]));

        ip1 = index_from_indices(z, y+1, x, Nz, Ny, Nx);
        im1 = index_from_indices(z, y-1, x, Nz, Ny, Nx);
        prefactor = factor*oody2;

        field[i] +=  prefactor * (mobility[ip1]*psi[ip1]*(mu[ip1]-mu[i]) + mobility[im1]*psi[im1]*(mu[im1]-mu[i]) + mobility[i]*psi[i]*(mu[im1]-2*mu[i]+mu[ip1]));
        field[i] += -prefactor * (mobility[ip1]*mu[ip1]*(psi[ip1]-psi[i]) + mobility[im1]*mu[im1]*(psi[im1]-psi[i]) + mobility[i]*mu[i]*(psi[im1]-2*psi[i]+psi[ip1]));

        ip1 = index_from_indices(z, y, x+1, Nz, Ny, Nx);
        im1 = index_from_indices(z, y, x-1, Nz, Ny, Nx);
        prefactor = factor*oodx2;

        field[i] +=  prefactor * (mobility[ip1]*psi[ip1]*(mu[ip1]-mu[i]) + mobility[im1]*psi[im1]*(mu[im1]-mu[i]) + mobility[i]*psi[i]*(mu[im1]-2*mu[i]+mu[ip1]));
        field[i] += -prefactor * (mobility[ip1]*mu[ip1]*(psi[ip1]-psi[i]) + mobility[im1]*mu[im1]*(psi[im1]-psi[i]) + mobility[i]*mu[i]*(psi[im1]-2*psi[i]+psi[ip1]));
    }
}

__global__ void print_field_from_device(udm_scalar *field, int Nfull)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nfull )
    {
        printf("%.1e ", field[i]);
    }
}

__global__ void find_maximum_kernel(udm_scalar* maximum, udm_scalar* field, int size)
{
        extern __shared__ udm_scalar shared[];
            
        int tid = threadIdx.x + blockDim.x * blockIdx.x;
                
        if(tid < size)
            shared[threadIdx.x] = field[tid];
        else //Thread is beyond data, initialize shared data to maximum possible value
            shared[threadIdx.x] = FLT_MAX;
        __syncthreads();
                            
        for(unsigned int s=blockDim.x/2; s>0; s>>=1) 
        {
            if (threadIdx.x < s &&  tid+s < size) 
            {
                shared[threadIdx.x] = fmax(shared[threadIdx.x], shared[threadIdx.x + s]);
            }
            __syncthreads();
        }
                              
        if(threadIdx.x == 0)
            maximum[blockIdx.x] = shared[0];
}

__global__ void calculate_density_dependent_mobilty_kernel(udm_scalar *mobility, udm_scalar factor, udm_scalar *ddm_parameters, udm_scalar *phi, int n_components, int Nreal)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if( i < Nreal )
    {
         udm_scalar deltamaphi = ddm_parameters[0];
         for(int j=0; j<n_components; j++)
         {
             deltamaphi -= ddm_parameters[j+1] * phi[j*Nreal+i];
         }
         //factor saves c->m/2.
         mobility[i] = factor * (1 + tanh(deltamaphi));
    }
}
