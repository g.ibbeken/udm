/* Copyright (c) 2020-2024 Gregor Haefner

   This file is part of UDM.

   UDM is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   UDM is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with UDM.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "helper.h"
#include "phase.h"
#include "kernels.h"
#include "saving.h"

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <curand.h>
#include <curand_kernel.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BINS 201
#define MAXBIN 4.0 


/*---Functions: ---------------------------------------------------------------------------------------------------------------------------------------------------*/
udm_scalar average(PHASE *p, udm_scalar *vec)
    {
        udm_scalar av=0;
        for(int i=0; i<p->Nreal; i++)
            av += vec[i];
        return av/(udm_scalar)p->Nreal;
    }

udm_scalar maxabs(PHASE *p, udm_scalar *vec)
    {
        udm_scalar max=0;
        for(int i=0; i<p->Nreal; i++)
        {
            udm_scalar absv = abs(vec[i]);
            udm_scalar current_max;
            current_max = max;
            if (absv > current_max)
                max = absv;
        }
        return max;
    }

void reevaluate_averages(PHASE *p)
{
    for(int n1=0; n1<p->n_components; n1++)
    {   
        p->comps[n1]->rho = average(p, p->phi+n1*p->Nreal);//
    }
}

udm_scalar root_mean_square(PHASE *p, udm_scalar *vec)
    {
        udm_scalar rms=0;
        udm_scalar av = average(p, vec);
        for(int i=0;i<p->Nreal;i++)
            rms+= (vec[i]-av)*(vec[i]-av);
        return sqrt(rms/p->Nreal);
    }

void nan_test(udm_scalar *host, udm_scalar *device, int size)
{
    //copy back fields phi from device
    HANDLE_ERROR(cudaMemcpy(host, device, size*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    for(int i=0; i<size; i++)
    {
        if (isnan(host[i])){
            printf("Nan detected!\n");
            exit(1);
        }
    }
}

void copy_array(udm_scalar *array, udm_scalar *target, int size)
{
    for(int i=0; i<size; i++)
        target[i] = array[i];
}

void find_solution_start(PHASE *p)
{
#ifdef ENABLE_EVAPORATION_STABILIZATION
    HANDLE_ERROR(cudaMemcpy(p->phi, p->_phi, p->Nfull*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    COMPONENT *ca=p->comps[0];
    COMPONENT *cb=p->comps[1];
    COMPONENT *cg=p->comps[p->n_components-1];
    udm_scalar avg, avp;
    int conversion_delay = p->conversion_delay;
    int start_c = p->interface_position-1;
    do
    {
        avg = avp = 0;
        start_c++;
        for(unsigned int i=start_c*p->Nslice; i<(start_c+1)*p->Nslice; i++)
        {
            avg += cg->phi[i];
            avp += ca->phi[i]+cb->phi[i];
        }
    } while(avg>avp);
    if( start_c > p->interface_position )
    {
        p->interface_position = start_c;
        if(start_c>conversion_delay)
        {
            for(int iconv=0; iconv<p->n_conversions; iconv++)
            {
                CONVERSION *cv = p->convs[iconv];
                if (cv->type == custom)
                {
                    int ncells = p->Nslice*(start_c-conversion_delay);
                    printf("Widening conversion zone to width %d.\n", start_c-conversion_delay);
                    initialize_value_kernel<<<p->blocks_single, p->threads>>>(cv->_custom_domain, (uint8_t)1, ncells);
#ifndef ENABLE_REFLECTING_BC
                    initialize_value_kernel<<<p->blocks_single, p->threads>>>(cv->_custom_domain+p->Nreal-ncells, (uint8_t)1, ncells);
#endif //ENABLE_REFLECTING_BC
                    HANDLE_ERROR(cudaMemcpy(cv->custom_domain, cv->_custom_domain, p->Nreal*sizeof(uint8_t), cudaMemcpyDeviceToHost));
                    write_custom_domain(p, cv);
                }
            }
        }
    }
#endif //ENABLE_EVAPORATION_STABILIZATION
}

void find_solution_start_original(PHASE *p) //Used for first THREE components, a more versatile use of this tool still needs to be implemented!!!
{
#ifdef ENABLE_EVAPORATION_STABILIZATION
    HANDLE_ERROR(cudaMemcpy(p->phi, p->_phi, p->Nfull*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    COMPONENT *cg=p->comps[p->n_components-1];
    udm_scalar av;
    int conversion_delay = p->conversion_delay;
    int start_c = p->interface_position-1;
    do
    {
        av=0;
        start_c++;
        for(unsigned int i=start_c*p->Nslice; i<(start_c+1)*p->Nslice; i++)
        {
            av += cg->phi[i];// + cb->phi[i] ;
        }
        av /= p->Nslice;
    } while(av>0.5);
    if(start_c>conversion_delay)
    {
        p->interface_position = conversion_delay;
        p->shift += start_c-conversion_delay;
        shift_fields(p, start_c-conversion_delay, p->mu);
        printf("Phase boundary has moved by:                                                                                                   %d\n", p->shift);
        //for(unsigned int i=0; i<(conversion_delay-10)*p->Nslice; i++)
        //{
        //    p->comps[0]->phi[i] = 0;//cb->phi[i] = 0;
        //}
        //for(unsigned int i=p->Nreal-(conversion_delay-10)*p->Nslice; i<p->Nreal; i++)
        //{
        //    p->comps[0]->phi[i] = 0;//cb->phi[i] = 0;
        //}
    //for(unsigned int i=(p->conversion_delay+40)*p->Nslice; i<p->Nreal-(conversion_delay+40)*p->Nslice; i++)
    //{
    //    p->comps[p->n_components-2]->phi[i] += p->comps[p->n_components-1]->phi[i];
    //    p->comps[p->n_components-1]->phi[i] = 0;
    //}
        HANDLE_ERROR(cudaMemcpy(p->_phi, p->phi, p->Nfull*sizeof(udm_scalar), cudaMemcpyHostToDevice));
    } else if( start_c > p->interface_position )
    {
        p->interface_position = start_c;
    }
#endif //ENABLE_EVAPORATION_STABILIZATION
}

void shift_fields(PHASE *p, int shift, udm_scalar *copy)
{
#ifdef ENABLE_EVAPORATION_STABILIZATION
    int fullshift = shift * p->Nslice;
    //copy field:
    for(int i=0; i<p->Nfull; i++)
        copy[i] = p->phi[i];
    //perform shift from copied array:
#ifndef ENABLE_REFLECTING_BC
    for(int ci=0; ci<p->n_components; ci++)
    {
        udm_scalar *phi_shifted = copy + fullshift + ci * p->Nreal;
        udm_scalar *phi = p->phi + ci * p->Nreal;
        for(int i=0; i<p->Nreal/2-fullshift; i++)
            phi[i] = phi_shifted[i];
        phi_shifted += p->Nreal/2 - fullshift;
        phi += p->Nreal/2 + fullshift;
        for(int i=0; i<p->Nreal/2-fullshift; i++)
            phi[i] = phi_shifted[i];

        phi -= 2 * fullshift;
        for(int i=0; i<2*fullshift; i++)
            phi[i] = p->comps[ci]->rhobulk;
    }
#else //ENABLE_REFLECTING_BC
    for(int ci=0; ci<p->n_components; ci++)
    {
        udm_scalar *phi_shifted = copy + fullshift + ci * p->Nreal;
        udm_scalar *phi = p->phi + ci * p->Nreal;
        for(int i=0; i<p->Nreal-fullshift; i++)
            phi[i] = phi_shifted[i];
        phi += p->Nreal - fullshift;
        for(int i=0; i<fullshift; i++)
            phi[i] = p->comps[ci]->rhobulk;
    }
#endif //ENABLE_REFLECTING_BC
#endif //ENABLE_EVAPORATION_STABILIZATION
}

void HandleError(cudaError_t err, char const * const file, int const line)
{
    if (err != cudaSuccess) {
        fprintf(stderr, "%s in %s at line %d\n", cudaGetErrorString(err), file, line);
        exit(EXIT_FAILURE);
    }
}

void HandleError(cufftResult err, char const * const file, int const line)
{
    if (err != CUFFT_SUCCESS) {
        switch(err) {
            case CUFFT_INVALID_PLAN:
                printf ("cufft %s in %s at line %d\n", "CUFFT_INVALID_PLAN", file, line);
                break;
            case CUFFT_INVALID_VALUE:
                printf ("cufft %s in %s at line %d\n", "CUFFT_INVALID_VALUE", file, line);
                break;
            case CUFFT_INTERNAL_ERROR:
                printf ("cufft %s in %s at line %d\n", "CUFFT_INTERNAL_ERROR", file, line);
                break;
            case CUFFT_EXEC_FAILED:
                printf ("cufft %s in %s at line %d\n", "CUFFT_EXEC_FAILED", file, line);
                break;
            case CUFFT_SETUP_FAILED:
                printf ("cufft %s in %s at line %d\n", "CUFFT_EXEC_FAILEDCUFFT_SETUP_FAILED", file, line);
                break;
            default:
                printf ("cufft %s in %s at line %d\n", "CUFFT_EXEC_FAILEDCUFFT_SETUP_FAILED", file, line);
        }
        exit(EXIT_FAILURE);
    }
}

void update_host(PHASE *p)
{
    HANDLE_ERROR(cudaMemcpy(p->phi, p->_phi, p->Nfull*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    HANDLE_ERROR(cudaMemcpy(p->mu, p->_mu, p->Nfull*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    HANDLE_ERROR(cudaMemcpy(p->fe, p->_fe, p->Nreal*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
}

void print_field(udm_scalar *host, udm_scalar *device, int n, const char* name)
{
//    //copy back fields phi from device
//    HANDLE_ERROR(cudaMemcpy(host, device, n*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
//
//    printf("%s: ", name);
//    for(int i=0;i<n;i++)
//        printf("%f ", host[i]);
//    printf("\n");
}

void print_means(PHASE *p, udm_scalar *field, const char *name)
{

    printf("%s means: ", name);
    HANDLE_ERROR(cudaMemcpy(p->mu, field, p->Nfull*sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    for(int ci=0; ci<p->n_components; ci++)
    {
        udm_scalar mean = average(p, p->mu+ci*p->Nreal);
        printf("%e ", mean);
    }
    printf("\n");
}

udm_scalar find_maximum(PHASE *p, udm_scalar *_field, int size)
{
    dim3 nblocks;
    nblocks.x = (size + p->threads.x - 1)/p->threads.x;
    nblocks.y = 1;
    nblocks.z = 1;

    size_t shared_mem_size = p->threads.x * sizeof(udm_scalar);

    udm_scalar *_block_maxima = p->_gpu_buffer0;
    udm_scalar *_block_maxima_cp = p->_gpu_buffer1;
    udm_scalar *_to_maximize = _field;

    while(nblocks.x > 1)
    {
        cudaDeviceSynchronize();
        find_maximum_kernel<<<nblocks, p->threads, shared_mem_size>>>(_block_maxima, _to_maximize, size);
        _to_maximize = _block_maxima;
        _block_maxima = _block_maxima_cp;
        _block_maxima_cp = _to_maximize;
        size = nblocks.x;
        nblocks.x = (nblocks.x + p->threads.x - 1)/p->threads.x;
    }
    find_maximum_kernel<<<nblocks, p->threads, shared_mem_size>>>(_block_maxima, _to_maximize, size);

    udm_scalar maximum;
    HANDLE_ERROR(cudaMemcpy(&maximum, _block_maxima, sizeof(udm_scalar), cudaMemcpyDeviceToHost));
    return maximum;
}
