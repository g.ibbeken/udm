#!/bin/bash -x
#SBATCH --job-name=udm
#SBATCH --account=psm
#SBATCH --nodes=1
#SBATCH --time=03:00:00
#SBATCH --partition=booster
#SBATCH --gres=gpu:4
#SBATCH --dependency=singleton

srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:0  ./UDM $1 &
srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:10 ./UDM $2 &
srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:20 ./UDM $3 &
srun --exclusive -n 1 --gres=gpu:1 --cpu-bind=map_cpu:30 ./UDM $4 &

wait
